<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// Logged out routes
Route::get('/', 'PagesController@index');
Route::get('/search', 'PagesController@search');

Route::get('courses', 'CoursesController@index');
Route::get('course/{id}', 'CoursesController@show');

Route::get('lessons', 'LessonsController@index');
Route::get('lesson/{id}', 'LessonsController@show');

Route::get('login', 'Auth\AuthController@getLogin');
Route::post('login', 'Auth\AuthController@postLogin');
Route::get('logout', 'Auth\AuthController@getLogout');

Route::get('register', 'Auth\AuthController@getRegister');
Route::post('register', 'Auth\AuthController@postRegister');

Route::get('password/email', 'Auth\PasswordController@getEmail');
Route::post('password/email', 'Auth\PasswordController@postEmail');

Route::get('password/reset/{token}', 'Auth\PasswordController@getReset');
Route::post('password/reset', 'Auth\PasswordController@postReset');


// Logged in routes
Route::group(['middleware' => 'auth'], function () {
    // Profile
    Route::get('profile', 'ProfileController@getProfile');
    Route::post('profile', 'ProfileController@postProfile');

    Route::post('profile/ezys', 'ProfileController@buyEzys');
    Route::post('profile/password', 'ProfileController@updatePassword');


    // Users
    Route::get('user/{id}', 'UsersController@show');
    Route::get('user/ezys/donate/{id}', 'UsersController@getDonateEzys');
    Route::post('user/ezys/donate', 'UsersController@donateEzys');


    // Admin
    // Manage users
    Route::get('admin/user/manage/{id}', 'AdminsController@userManage');
    Route::post('admin/user/manage', 'AdminsController@userManageProcess');
    Route::post('admin/user/delete', 'AdminsController@userDelete');

    // Manage notes
    Route::get('admin/note/manage/{id}', 'AdminsController@noteManage');
    Route::post('admin/note/manage', 'AdminsController@noteManageProcess');
    Route::post('admin/note/delete', 'AdminsController@noteDelete');

    // Manage lessons
    Route::get('admin/lesson/manage/{id}', 'AdminsController@lessonManage');
    Route::post('admin/lesson/manage', 'AdminsController@lessonManageProcess');
    Route::post('admin/lesson/delete', 'AdminsController@lessonDelete');
    Route::post('admin/comment/delete', 'AdminsController@commentDelete');

    // Manage courses
    Route::get('admin/course/manage/{id}', 'AdminsController@courseManage');
    Route::post('admin/course/manage', 'AdminsController@courseManageProcess');
    Route::post('admin/course/delete', 'AdminsController@courseDelete');

    // Lessons
    Route::get('profile/lessons', 'LessonsController@profileLessons');

    Route::get('profile/lesson/create', 'LessonsController@getProfileLessonCreate');
    Route::post('profile/lesson/create', 'LessonsController@postProfileLessonCreate');

    Route::get('profile/lesson/update/{id}', 'LessonsController@getProfileLessonUpdate');
    Route::post('profile/lesson/update', 'LessonsController@postProfileLessonUpdate');

    Route::post('profile/lesson/delete', 'LessonsController@getProfileLessonDelete');

    Route::get('user/lessons/{id}', 'LessonsController@userLessons');

    Route::post('lesson/comment', 'CommentsController@postComment');

    Route::post('lesson/access', 'LessonsController@buyAccess');

    // Courses
    Route::get('profile/courses', 'CoursesController@profileCourses');

    Route::get('profile/course/create', 'CoursesController@getProfileCourseCreate');
    Route::post('profile/course/create', 'CoursesController@postProfileCourseCreate');

    Route::get('profile/course/update/{id}', 'CoursesController@getProfileCourseUpdate');
    Route::post('profile/course/update', 'CoursesController@postProfileCourseUpdate');

    Route::post('profile/course/delete', 'CoursesController@getProfileCourseDelete');
    Route::post('profile/course/lesson/delete', 'CoursesController@getProfileCourseLessonDelete');

    Route::get('user/courses/{id}', 'CoursesController@userCourses');

    Route::post('course/access', 'CoursesController@buyAccess');


    // Notes
    Route::get('profile/notes', 'NotesController@profileNotes');

    Route::get('profile/note/create', 'NotesController@getProfileNoteCreate');
    Route::post('profile/note/create', 'NotesController@postProfileNoteCreate');

    Route::get('profile/note/update/{id}', 'NotesController@getProfileNoteUpdate');
    Route::post('profile/note/update', 'NotesController@postProfileNoteUpdate');

    Route::post('profile/note/delete', 'NotesController@getProfileNoteDelete');

    Route::get('user/notes/{id}', 'NotesController@userNotes');
});
