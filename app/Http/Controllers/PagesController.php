<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Auth;


class PagesController extends Controller {
    public function index() {
        return view('pages.index');
    }

    public function search() {
        return view('pages.search');
    }
}
