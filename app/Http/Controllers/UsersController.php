<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Auth;

use App\Comment as Comment;
use App\Course as Course;
use App\Lesson as Lesson;
use App\User as User;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;

class UsersController extends Controller
{
    public function show($id) {
        if(Auth::user()->role == 'suspended') {
            return view('profile.suspended');
        }

        // Find user by ID
        $userProfile = User::find($id);

        if(!$userProfile) {
            $userProfile = false;
        }

        return view('users.show', compact('userProfile'));
    }

    public function getDonateEzys($id) {
        if(Auth::user()->role == 'suspended') {
            return view('profile.suspended');
        }

        // Find user by ID
        $userProfile = User::find($id);

        if(!$userProfile) {
            $userProfile = false;
        }

        return view('users.donateEzys', compact('userProfile'));
    }

    public function donateEzys() {
        // Get data from ajax request
        $ezys                  = Input::get('ezys');
        $userID                = Input::get('userID');

        $errorString = '';
        $errorArray = array();

        if(empty($ezys)) {
            $errorString .= 'Please fill in ezys amount!<br />';
            array_push($errorArray, 'ezys');
        }

        if(Auth::user()->role == 'suspended') {
            $errorString .= 'You may not complete this action because your account has been suspended!<br />';
        }

        $donorUser = User::find(Auth::user()->id);
        $profileUser = User::find($userID);

        if(Auth::user()->id == $userID) {
            $errorString .= 'You can not donate ezys to yourself!<br />';
        }

        if(!$profileUser) {
            $errorString .= 'This user does not exist!<br />';
        }

        if($donorUser->ezys < $ezys) {
            $errorString .= 'You do not have that many ezys to donate!<br />';
        }

        if(!empty($errorString)) {
            return json_encode(array('message' => $errorString, 'errorLocation' => $errorArray));
        } else {
            $profileUser->ezys += $ezys;

            $profileUser->save();

            $donorUser->ezys -= $ezys;

            $donorUser->save();

            return 'success';
        }
    }
}
