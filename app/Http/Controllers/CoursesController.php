<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Auth;


use App\Course as Course;
use App\Lesson as Lesson;

use Illuminate\Support\Facades\DB;
use App\User as User;
use App\Access as Access;

use App\Comment as Comment;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;

class CoursesController extends Controller
{
    // Publicly accessed
    public function index($paginate = 12) {
        // Get all public courses.
        $courses = Course::where(['privacy' => 1])->orderBy('created_at', 'desc')->paginate($paginate);
        $total_courses = Course::where(['privacy' => 1])->orderBy('created_at', 'desc')->get();

        $total_returned = count($courses);
        $total_courses = count($total_courses);

        $lessons = 1;

        $place_action = 'archive';

        $userName = '';

        return view('courses.index', compact(['courses', 'lessons', 'total_courses', 'total_returned', 'place_action', 'userName']));
    }

    public function show($id) {
        // Find course by ID
        $course = Course::find($id);
        $lessons = false;
        $author = false;
        $accessToCourse = false;

        if(!$course) {
            $course = false;
        } else {
            $lessons = Lesson::where(['courseID' => $id])->orderBy('created_at', 'desc')->get();

            $author = User::find($course->authorID);

            if(!$author) {
                $author = false;
            } else {
                $author = $author->name;
            }

            $user =  Auth::user();

            if($user) {
                if($course->authorID == $user->id OR $user->role == 'moderator' OR $user->role == 'administrator') {
                    $accessToCourse = true;
                }
            }

            if(!$accessToCourse) {
                if($course->type == '0') {
                    $accessToCourse = true;
                } else {
                    if($user) {
                        $access = Access::where(['courseID' => $id, 'userID' => $user->id])->get();

                        if(count($access) > 0) {
                            $accessToCourse = true;
                        }

                        if(Auth::user()->role == 'suspended') {
                            $accessToCourse = false;
                        }
                    }
                }
            }
        }

        return view('courses.show', compact('course', 'lessons', 'author', 'accessToCourse'));
    }


    // Profile access
    public function profileCourses($paginate = 12) {
        if(Auth::user()->role == 'suspended') {
            return view('profile.suspended');
        }

        $user =  Auth::user();

        $courses = Course::where(['authorID' => $user->id])->orderBy('created_at', 'desc')->paginate($paginate);
        $total_courses = Course::where(['authorID' => $user->id])->orderBy('created_at', 'desc')->get();

        $total_returned = count($courses);
        $total_courses = count($total_courses);

        $lessons = 1;

        $place_action = 'profile';

        $userName = '';

        return view('courses.index', compact(['courses', 'lessons', 'total_courses', 'total_returned', 'place_action', 'userName']));
    }


    // User lessons
    public function userCourses($userID, $paginate = 12) {
        if(Auth::user()->role == 'suspended') {
            return view('profile.suspended');
        }

        $user =  User::find($userID);

        $courses = Course::where(['authorID' => $user->id])->orderBy('created_at', 'desc')->paginate($paginate);
        $total_courses = Course::where(['authorID' => $user->id])->orderBy('created_at', 'desc')->get();

        $total_returned = count($courses);
        $total_courses = count($total_courses);

        $lessons = 1;

        $place_action = 'user';

        $userName = $user->name;

        return view('courses.index', compact(['courses', 'lessons', 'total_courses', 'total_returned', 'place_action', 'userName']));
    }

    public function getProfileCourseCreate() {
        if(Auth::user()->role == 'suspended') {
            return view('profile.suspended');
        }

        return view('courses.create');
    }

    public function postProfileCourseCreate() {
        // Get data from ajax request
        $title                  = Input::get('title');
        $excerpt                = Input::get('excerpt');
        $thumbnail              = Input::get('thumbnail');
        $type                   = Input::get('type');
        $price                  = Input::get('price');
        $tag                    = Input::get('tag');

        $errorString = '';
        $errorArray = array();

        if(empty($title)) {
            $errorString .= 'Please fill in title!<br />';
            array_push($errorArray, 'title');
        }

        if(empty($excerpt)) {
            $errorString .= 'Please fill in excerpt!<br />';
            array_push($errorArray, 'excerpt');
        }

        if($type == '1' && empty($price)) {
            $errorString .= 'Price may not be empty if course type is paid!<br />';
            array_push($errorArray, 'price');
        }

        if(Auth::user()->role == 'suspended') {
            $errorString .= 'You may not complete this action because your account has been suspended!<br />';
        }

        if(!empty($errorString)) {
            return json_encode(array('message' => $errorString, 'errorLocation' => $errorArray));
        } else {
            $newCourse = new Course;
            $newCourse->title      = $title;
            $newCourse->excerpt    = $excerpt;

            if(empty($thumbnail)) {
                $newCourse->thumbnail = null;
            } else {
                $newCourse->thumbnail = $thumbnail;
            }

            $newCourse->type       = $type;

            if($type == 1) {
                $newCourse->price  = $price;
            } else {
                $newCourse->price  = 0;
            }

            if(!empty($tag)) {
                $newCourse->tag = $tag;
            }

            $newCourse->privacy    = 0;

            $newCourse->authorID   = Auth::user()->id;

            $newCourse->save();

            return 'success';
        }
    }

    public function getProfileCourseUpdate($id) {
        $course = Course::find($id);

        if(Auth::user()->role == 'suspended') {
            return view('profile.suspended');
        } else if(!$course) {
            return Redirect::to('profile/courses');
        } else if($course->authorID != Auth::user()->id) {
            return Redirect::to('profile/courses');
        } else {
            $lessons = Lesson::where(['courseID' => $id])->orderBy('created_at', 'desc')->get();

            return view('courses.update', compact(['course', 'lessons']));
        }
    }

    public function postProfileCourseUpdate() {
        // Get data from ajax request
        $courseID               = Input::get('courseID');
        $title                  = Input::get('title');
        $excerpt                = Input::get('excerpt');
        $thumbnail              = Input::get('thumbnail');
        $tag                    = Input::get('tag');

        $updateCourse = Course::find($courseID);

        $errorString = '';
        $errorArray = array();

        if($updateCourse->authorID != Auth::user()->id) {
            $errorString .= 'This course does not belong to you!<br />';
        } else {
            if(empty($title)) {
                $errorString .= 'Please fill in title!<br />';
                array_push($errorArray, 'title');
            }

            if(empty($excerpt)) {
                $errorString .= 'Please fill in excerpt!<br />';
                array_push($errorArray, 'excerpt');
            }
        }

        if(Auth::user()->role == 'suspended') {
            $errorString .= 'You may not complete this action because your account has been suspended!<br />';
        }

        if(!empty($errorString)) {
            return json_encode(array('message' => $errorString, 'errorLocation' => $errorArray));
        } else {
            $updateCourse->title      = $title;
            $updateCourse->excerpt    = $excerpt;

            if(empty($thumbnail)) {
                $updateCourse->thumbnail = null;
            } else {
                $updateCourse->thumbnail = $thumbnail;
            }

            if(!empty($tag)) {
                $updateCourse->tag = $tag;
            }

            $updateCourse->save();

            return 'success';
        }
    }

    public function getProfileCourseDelete() {
        $courseID  = Input::get('courseID');

        $course = Course::find($courseID);

        if(Auth::user()->role == 'suspended') {
            return 'You may not complete this action because your account has been suspended!<br />';
        } else if(!$course) {
            return 'An error has occurred while trying to delete course.';
        } else if($course->authorID != Auth::user()->id) {
            return 'This course does not belong to you';
        } else {
            // Delete course + its content
            // Course's accessess
            $courseAccesses = Access::where(['courseID' => $course->id])->get();

            foreach($courseAccesses as $courseAccess) {
                $courseAccess->forceDelete();
            }

            // Course's lessons
            $courseLessons = Lesson::where(['courseID' => $course->id])->get();

            foreach($courseLessons as $courseLesson) {
                // Lesson's accessess
                $courseLessonAccesses = Access::where(['lessonID' => $courseLesson->id])->get();

                foreach($courseLessonAccesses as $courseLessonAccess) {
                    $courseLessonAccess->forceDelete();
                }

                // Lesson's comments
                $courseLessonComments = Comment::where(['lessonID' => $courseLesson->id])->get();

                foreach($courseLessonComments as $courseLessonComment) {
                    $courseLessonComment->forceDelete();
                }

                $courseLesson->forceDelete();
            }

            // Course
            $course->forceDelete();

            return 'success';
        }
    }

    public function getProfileCourseLessonDelete() {
        $courseID               = Input::get('courseID');
        $lessonID               = Input::get('lessonID');

        $course = Course::find($courseID);
        $lesson = Lesson::find($lessonID);

        if(Auth::user()->role == 'suspended') {
            return 'You may not complete this action because your account has been suspended!<br />';
        } else if(!$course OR !$lesson) {
            return 'An error has occurred while trying to remove lesson from the course.';
        } else if($course->authorID != Auth::user()->id OR $lesson->authorID != Auth::user()->id) {
            return 'This course or lesson does not belong to you';
        } else {
            $lesson->courseID   = null;

            $lesson->save();

            return 'success';
        }
    }

    public function buyAccess() {
        if(Auth::user()->role == 'suspended') {
            return 'You may not complete this action because your account has been suspended!<br />';
        }

        $courseID               = Input::get('courseID');

        $course = Course::find($courseID);
        $user = Auth::user();

        $access = Access::where(['courseID' => $courseID, 'userID' => $user->id])->get();

        if(!$course OR !$user) {
            return 'An error has occurred while buying access to this course.';
        } else if($course->type == '0' OR $course->authorID == $user->id OR $user->role == 'moderator' OR $user->role == 'administrator' OR count($access) > 0) {
            return 'You already have access to this course!';
        } else if($user->ezys < $course->price) {
            return 'You do not have enough ezys to pay for the access of this course!';
        } else {
            $author = User::find($course->authorID);
            $author->ezys += $course->price;

            $author->save();

            $user->ezys -= $course->price;
            $user->save();

            $newAccess = new Access;
            $newAccess->courseID = $courseID;
            $newAccess->userID = $user->id;

            $newAccess->save();

            return 'success';
        }
    }
}
