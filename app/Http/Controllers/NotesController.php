<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Auth;

use App\User as User;
use App\Lesson as Lesson;
use App\Note as Note;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;


class NotesController extends Controller
{
    // Profile access
    public function profileNotes($paginate = 12) {
        if(Auth::user()->role == 'suspended') {
            return view('profile.suspended');
        }

        $user =  Auth::user();

        $notes = Note::where(['authorID' => $user->id])->orderBy('created_at', 'desc')->paginate($paginate);
        $total_notes = Note::where(['authorID' => $user->id])->orderBy('created_at', 'desc')->get();

        $total_returned = count($notes);
        $total_notes = count($total_notes);

        $place_action = 'profile';

        $userName = '';

        return view('notes.index', compact(['notes', 'total_notes', 'total_returned', 'place_action', 'userName']));
    }

    public function userNotes($userID, $paginate = 12) {
        if(Auth::user()->role == 'suspended') {
            return view('profile.suspended');
        }

        $user =  User::find($userID);

        if(Auth::user()->role == 'administrator') {
            $notesArgs = array('authorID' => $user->id);
        } else {
            $notesArgs = array('authorID' => $user->id, 'privacy' => 1);
        }

        $notes = Note::where($notesArgs)->orderBy('created_at', 'desc')->paginate($paginate);
        $total_notes = Note::where($notesArgs)->orderBy('created_at', 'desc')->get();

        $total_returned = count($notes);
        $total_notes = count($total_notes);

        $place_action = 'user';

        $userName = $user->name;

        return view('notes.index', compact(['notes', 'total_notes', 'total_returned', 'place_action', 'userName']));
    }

    public function getProfileNoteCreate() {
        if(Auth::user()->role == 'suspended') {
            return view('profile.suspended');
        }

        return view('notes.create');
    }

    public function postProfileNoteCreate() {
        // Get data from ajax request
        $note                  = Input::get('note');
        $privacy               = Input::get('privacy');
        $lessonID              = Input::get('lessonID');

        $lesson = Lesson::find($lessonID);

        $errorString = '';
        $errorArray = array();

        if(!empty($lessonID) && !$lesson) {
            $errorString .= 'This lesson does not exist!<br />';
        }

        if(empty($note)) {
            $errorString .= 'Please fill in note!<br />';
            array_push($errorArray, 'note');
        }

        if(Auth::user()->role == 'suspended') {
            $errorString .= 'You may not complete this action because your account has been suspended!<br />';
        }

        if(!empty($errorString)) {
            return json_encode(array('message' => $errorString, 'errorLocation' => $errorArray));
        } else {
            $newNote = new Note;
            $newNote->note      = $note;
            $newNote->privacy   = $privacy;

            $newNote->authorID   = Auth::user()->id;

            if($lessonID) {
                $newNote->lessonID   = $lessonID;
            } else {
                $newNote->lessonID   = null;
            }

            $newNote->save();

            return 'success';
        }
    }


    public function getProfileNoteUpdate($id) {
        if(Auth::user()->role == 'suspended') {
            return view('profile.suspended');
        }

        $note = Note::find($id);

        if(!$note) {
            return Redirect::to('profile/notes');
        } else if($note->authorID != Auth::user()->id) {
            return Redirect::to('profile/notes');
        } else {
            return view('notes.update', compact(['note']));
        }
    }

    public function postProfileNoteUpdate() {
        // Get data from ajax request
        $noteID                 = Input::get('noteID');
        $note                   = Input::get('note');
        $privacy                = Input::get('privacy');

        $updateNote = Note::find($noteID);

        $errorString = '';
        $errorArray = array();

        if($updateNote->authorID != Auth::user()->id) {
            $errorString .= 'This note does not belong to you!<br />';
        } else {
            if(empty($note)) {
                $errorString .= 'Please fill in note!<br />';
                array_push($errorArray, 'note');
            }
        }

        if(Auth::user()->role == 'suspended') {
            $errorString .= 'You may not complete this action because your account has been suspended!<br />';
        }

        if(!empty($errorString)) {
            return json_encode(array('message' => $errorString, 'errorLocation' => $errorArray));
        } else {
            $updateNote->note       = $note;
            $updateNote->privacy    = $privacy;

            $updateNote->save();

            return 'success';
        }
    }

    public function getProfileNoteDelete() {
        $noteID  = Input::get('noteID');

        $note = Note::find($noteID);

        if(Auth::user()->role == 'suspended') {
            return 'You may not complete this action because your account has been suspended!<br />';
        } else if(!$note) {
            return 'An error has occurred while trying to delete note.';
        } else if($note->authorID != Auth::user()->id) {
            return 'This note does not belong to you';
        } else {
            $note->forceDelete();

            return 'success';
        }
    }
}
