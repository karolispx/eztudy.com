<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Course as Course;
use App\Lesson as Lesson;
use App\User as User;

use App\Comment as Comment;
use App\Access as Access;
use App\Note as Note;

use Illuminate\Support\Facades\Auth;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;

class AdminsController extends Controller
{
    public function courseManage($id) {
        if(Auth::user()->role != 'moderator' && Auth::user()->role != 'administrator') {
            return view('profile.noright');
        }

        $course = Course::find($id);

        if(!$course) {
            $course = false;
        }

        return view('admin.course', compact(['course']));
    }

    public function courseManageProcess() {
        // Get data from ajax request
        $courseID                   = Input::get('courseID');
        $title                      = Input::get('title');
        $excerpt                    = Input::get('excerpt');
        $thumbnail                  = Input::get('thumbnail');
        $privacy                    = Input::get('privacy');
        $tag                        = Input::get('tag');

        $errorString = '';
        $errorArray = array();

        if(empty($title)) {
            $errorString .= 'Please fill in all title!<br />';
            array_push($errorArray, 'title');
        }

        if(empty($excerpt)) {
            $errorString .= 'Please fill in all excerpt!<br />';
            array_push($errorArray, 'excerpt');
        }

        if($privacy != '1' && $privacy != '0') {
            $errorString .= 'Course privacy selected is wrong!<br />';
        }

        $managedCourse = Course::find($courseID);

        if(!$managedCourse) {
            $errorString .= 'This course does not exist!<br />';
        }

        if(Auth::user()->role != 'moderator' && Auth::user()->role != 'administrator') {
            $errorString .= 'You do not have the right to perform this action!<br />';
        }

        if(!empty($errorString)) {
            return json_encode(array('message' => $errorString, 'errorLocation' => $errorArray));
        } else {
            // Update information
            $managedCourse->title = $title;
            $managedCourse->excerpt = $excerpt;

            if(empty($thumbnail)) {
                $managedCourse->thumbnail = null;
            } else {
                $managedCourse->thumbnail = $thumbnail;
            }

            if(!empty($tag)) {
                $managedCourse->tag = $tag;
            }

            $managedCourse->privacy = $privacy;

            $managedCourse->save();

            return 'success';
        }
    }

    public function courseDelete() {
        // Get data from ajax request
        $courseID                  = Input::get('courseID');

        $course = Course::find($courseID);

        if(!$courseID) {
            return 'This course does not exist!';
        } else if(User::find($course->authorID)->role == 'administrator') {
            return 'You may not perform this action on this user!';
        } else if(Auth::user()->role != 'administrator') {
            return 'You do not have the right to perform this action!';
        } else {
            // Delete course + its content
            // Course's accessess
            $courseAccesses = Access::where(['courseID' => $course->id])->get();

            foreach($courseAccesses as $courseAccess) {
                $courseAccess->forceDelete();
            }

            // Course's lessons
            $courseLessons = Lesson::where(['courseID' => $course->id])->get();

            foreach($courseLessons as $courseLesson) {
                // Lesson's accessess
                $courseLessonAccesses = Access::where(['lessonID' => $courseLesson->id])->get();

                foreach($courseLessonAccesses as $courseLessonAccess) {
                    $courseLessonAccess->forceDelete();
                }

                // Lesson's comments
                $courseLessonComments = Comment::where(['lessonID' => $courseLesson->id])->get();

                foreach($courseLessonComments as $courseLessonComment) {
                    $courseLessonComment->forceDelete();
                }

                $courseLesson->forceDelete();
            }

            // Course
            $course->forceDelete();

            return 'success';
        }
    }

    public function lessonManage($id) {
        if(Auth::user()->role != 'moderator' && Auth::user()->role != 'administrator') {
            return view('profile.noright');
        }

        $lesson = Lesson::find($id);

        if(!$lesson) {
            $lesson = false;
        }

        $courses = Course::where(['authorID' => $lesson->authorID])->orderBy('created_at', 'desc')->get();

        return view('admin.lesson', compact(['lesson', 'courses']));
    }

    public function lessonManageProcess() {
        // Get data from ajax request
        $lessonID                   = Input::get('lessonID');
        $title                      = Input::get('title');
        $excerpt                    = Input::get('excerpt');
        $thumbnail                  = Input::get('thumbnail');
        $lesson                     = Input::get('lesson');
        $privacy                    = Input::get('privacy');
        $tag                        = Input::get('tag');

        $errorString = '';
        $errorArray = array();

        if(empty($title)) {
            $errorString .= 'Please fill in all title!<br />';
            array_push($errorArray, 'title');
        }

        if(empty($excerpt)) {
            $errorString .= 'Please fill in all excerpt!<br />';
            array_push($errorArray, 'excerpt');
        }

        if(empty($lesson)) {
            $errorString .= 'Please fill in all lesson!<br />';
            array_push($errorArray, 'lesson');
        }

        if($privacy != '1' && $privacy != '0') {
            $errorString .= 'Lesson privacy selected is wrong!<br />';
        }

        $managedLesson = Lesson::find($lessonID);

        if(!$managedLesson) {
            $errorString .= 'This lesson does not exist!<br />';
        }

        if(Auth::user()->role != 'moderator' && Auth::user()->role != 'administrator') {
            $errorString .= 'You do not have the right to perform this action!<br />';
        }

        if(!empty($errorString)) {
            return json_encode(array('message' => $errorString, 'errorLocation' => $errorArray));
        } else {
            // Update information
            $managedLesson->title = $title;
            $managedLesson->excerpt = $excerpt;

            if(empty($thumbnail)) {
                $managedLesson->thumbnail = null;
            } else {
                $managedLesson->thumbnail = $thumbnail;
            }

            if(!empty($tag)) {
                $managedLesson->tag = $tag;
            }

            $managedLesson->body = $lesson;
            $managedLesson->privacy = $privacy;

            $managedLesson->save();

            return 'success';
        }
    }

    public function commentDelete() {
        // Get data from ajax request
        $commentID                  = Input::get('commentID');

        $comment = Comment::find($commentID);

        if(!$comment) {
            return 'This comment does not exist!';
        } else if(Auth::user()->role != 'moderator' && Auth::user()->role != 'administrator') {
            return 'You do not have the right to perform this action!';
        } else {
            // Delete comment
            $comment->forceDelete();

            return 'success';
        }
    }

    public function lessonDelete() {
        // Get data from ajax request
        $lessonID                  = Input::get('lessonID');

        $lesson = Lesson::find($lessonID);

        if(!$lesson) {
            return 'This lesson does not exist!';
        } else if(User::find($lesson->authorID)->role == 'administrator') {
            return 'You may not perform this action on this user!';
        } else if(Auth::user()->role != 'administrator') {
            return 'You do not have the right to perform this action!';
        } else {
            // Delete lesson + its content
            // Lesson's accessess
            $lessonAccesses = Access::where(['lessonID' => $lesson->id])->get();

            foreach($lessonAccesses as $lessonAccess) {
                $lessonAccess->forceDelete();
            }

            // Lesson's comments
            $lessonComments = Comment::where(['lessonID' => $lesson->id])->get();

            foreach($lessonComments as $lessonComment) {
                $lessonComment->forceDelete();
            }

            $lesson->forceDelete();

            return 'success';
        }
    }


    public function noteManage($id) {
        if(Auth::user()->role != 'moderator' && Auth::user()->role != 'administrator') {
            return view('profile.noright');
        }

        $note = Note::find($id);

        if(!$note) {
            $note = false;
        }

        return view('admin.note', compact(['note']));
    }


    public function noteManageProcess() {
        // Get data from ajax request
        $noteID                  = Input::get('noteID');
        $note                    = Input::get('note');
        $privacy                    = Input::get('privacy');

        $errorString = '';
        $errorArray = array();

        if(empty($note)) {
            $errorString .= 'Please fill in all information!<br />';
            array_push($errorArray, 'note');
        }

        $managedNote = Note::find($noteID);

        if(!$managedNote) {
            $errorString .= 'This note does not exist!<br />';
        }

        if(Auth::user()->role != 'administrator') {
            $errorString .= 'You do not have the right to perform this action!<br />';
        }

        if($privacy != '1' && $privacy != '0') {
            $errorString .= 'Note privacy selected is wrong!<br />';
        }

        if(!empty($errorString)) {
            return json_encode(array('message' => $errorString, 'errorLocation' => $errorArray));
        } else {
            // Update information
            $managedNote->note = $note;
            $managedNote->privacy = $privacy;
            $managedNote->save();

            return 'success';
        }
    }


    public function noteDelete() {
        // Get data from ajax request
        $noteID                  = Input::get('noteID');

        $note = Note::find($noteID);

        if(!$note) {
            return 'This note does not exist!';
        } else if(User::find($note->authorID)->role == 'administrator') {
            return 'You may not perform this action on this user!';
        } else if(Auth::user()->role != 'administrator') {
            return 'You do not have the right to perform this action!';
        } else {
            // Delete note
            $note->forceDelete();

            return 'success';
        }
    }





    public function userManage($id) {
        if(Auth::user()->role != 'moderator' && Auth::user()->role != 'administrator') {
            return view('profile.noright');
        }

        $userProfile = User::find($id);

        if(!$userProfile) {
            $userProfile = false;
        }

        return view('admin.user', compact(['userProfile']));
    }

    public function userManageProcess() {
        // Get data from ajax request
        $userID                  = Input::get('userID');
        $ezys                    = Input::get('ezys');
        $role                    = Input::get('role');

        $errorString = '';
        $errorArray = array();

        if(empty($ezys)) {
            $errorString .= 'Please fill in all information!<br />';
            array_push($errorArray, 'ezys');
        }

        $managedUser = User::find($userID);

        if(!$managedUser) {
            $errorString .= 'This user does not exist!<br />';
        }

        if($managedUser->role == 'administrator') {
            $errorString .= 'You do not have the right to perform this action!<br />';
        }

        if(Auth::user()->role != 'moderator' && Auth::user()->role != 'administrator') {
            $errorString .= 'You do not have the right to perform this action!<br />';
        }

        if(Auth::user()->role == 'moderator') {
            if($role != 'user' && $role != 'suspended') {
                $errorString .= 'Wrong user role selected!<br />';
            }
        }

        if(Auth::user()->role == 'administrator') {
            if($role != 'user' && $role != 'suspended' && $role != 'moderator' && $role != 'administrator') {
                $errorString .= 'Wrong user role selected!<br />';
            }
        }

        if(!empty($errorString)) {
            return json_encode(array('message' => $errorString, 'errorLocation' => $errorArray));
        } else {
            // Update information
            $managedUser->role = $role;
            $managedUser->ezys = $ezys;
            $managedUser->save();

            return 'success';
        }
    }

    public function userDelete() {
        // Get data from ajax request
        $userID                  = Input::get('userID');

        $user = User::find($userID);

        if(!$user) {
            return 'This user does not exist!';
        } else if($user->role == 'administrator') {
            return 'You may not perform this action on this user!';
        } else if(Auth::user()->role != 'administrator') {
            return 'You do not have the right to perform this action!';
        } else {
            // Delete user + its content
            // Accesses
            $accesses = Access::where(['userID' => $userID])->get();

            foreach($accesses as $access) {
                $access->forceDelete();
            }

            // Comments
            $comments = Comment::where(['authorID' => $userID])->get();

            foreach($comments as $comment) {
                $comment->forceDelete();
            }

            // Courses
            $courses = Course::where(['authorID' => $userID])->get();

            foreach($courses as $course) {
                // Course's accessess
                $courseAccesses = Access::where(['courseID' => $course->id])->get();

                foreach($courseAccesses as $courseAccess) {
                    $courseAccess->forceDelete();
                }

                // Course's lessons
                $courseLessons = Lesson::where(['courseID' => $course->id])->get();

                foreach($courseLessons as $courseLesson) {
                    // Lesson's accessess
                    $courseLessonAccesses = Access::where(['lessonID' => $courseLesson->id])->get();

                    foreach($courseLessonAccesses as $courseLessonAccess) {
                        $courseLessonAccess->forceDelete();
                    }

                    // Lesson's comments
                    $courseLessonComments = Comment::where(['lessonID' => $courseLesson->id])->get();

                    foreach($courseLessonComments as $courseLessonComment) {
                        $courseLessonComment->forceDelete();
                    }

                    $courseLesson->forceDelete();
                }

                // Course
                $course->forceDelete();
            }

            // Lessons for this user
            $lessons = Lesson::where(['authorID' => $userID])->get();

            foreach($lessons as $lesson) {
                // Lesson's accessess
                $lessonAccesses = Access::where(['lessonID' => $lesson->id])->get();

                foreach($lessonAccesses as $lessonAccess) {
                    $lessonAccess->forceDelete();
                }

                // Lesson's comments
                $lessonComments = Comment::where(['lessonID' => $lesson->id])->get();

                foreach($lessonComments as $lessonComment) {
                    $lessonComment->forceDelete();
                }

                $lesson->forceDelete();
            }

            // Finally, delete user
            $user->forceDelete();

            return 'success';
        }
    }
}
