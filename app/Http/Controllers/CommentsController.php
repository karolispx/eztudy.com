<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Course as Course;
use App\Lesson as Lesson;
use App\User as User;

use App\Comment as Comment;
use App\Access as Access;


use Illuminate\Support\Facades\Auth;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;

class CommentsController extends Controller
{
    public function postComment() {
        // Get data from ajax request
        $commentField             = Input::get('comment');
        $lessonID                 = Input::get('lessonID');

        $errorString = '';
        $errorArray = array();

        $lesson = Lesson::find($lessonID);

        if(!$lesson) {
            $errorString .= 'This lesson does not exist!<br />';
        }

        if(empty($commentField)) {
            $errorString .= 'Please fill in comment!<br />';
            array_push($errorArray, 'title');
        }

        if(empty($lessonID)) {
            $errorString .= 'An error occured!<br />';
        }

        if(Auth::user()->role == 'suspended') {
            $errorString .= 'You may not complete this action because your account has been suspended!<br />';
        }

        if($lesson->courseID != null) {
            $course = Course::find($lesson->courseID);

            if(!$course) {
                $errorString .= 'An error occured when saving comment.<br />';
            } else {
                if($course->type == 1) {
                    $access = Access::where(['courseID' => $course->id, 'userID' => Auth::user()->id])->get();

                    if($course->authorID == Auth::user()->id OR Auth::user()->role == 'moderator' OR Auth::user()->role == 'administrator' OR count($access) > 0) {
                    } else {
                        $errorString .= 'You do not have access to this lesson!<br />';
                    }
                }
            }
        } else {
            if($lesson->type != 0) {
                $access = Access::where(['lessonID' => $lesson->id, 'userID' => Auth::user()->id])->get();

                if($lesson->authorID == Auth::user()->id OR Auth::user()->role == 'moderator' OR Auth::user()->role == 'administrator' OR count($access) > 0) {
                } else {
                    $errorString .= 'You do not have access to this lesson!<br />';
                }
            }
        }

        if(!empty($errorString)) {
            return json_encode(array('message' => $errorString, 'errorLocation' => $errorArray));
        } else {
            $comment = new Comment;
            $comment->comment    = $commentField;
            $comment->lessonID   = $lessonID;
            $comment->authorID   = Auth::user()->id;

            $author = User::find(Auth::user()->id);

            $comment->author   = $author->name;

            $comment->save();

            return 'success';
        }
    }
}
