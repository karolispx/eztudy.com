<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;

use App\User as User;

class ProfileController extends Controller
{
    public function getProfile() {
        return view('profile.index');
    }

    public function buyEzys() {
        // Get data from ajax request
        $ezys                  = Input::get('ezys');

        $errorString = '';
        $errorArray = array();

        if(empty($ezys)) {
            $errorString .= 'Please fill in ezys amount!<br />';
            array_push($errorArray, 'ezys');
        }

        if(Auth::user()->role == 'suspended') {
            $errorString .= 'You may not complete this action because your account has been suspended!<br />';
        }

        if(!empty($errorString)) {
            return json_encode(array('message' => $errorString, 'errorLocation' => $errorArray));
        } else {
            $user = User::find(Auth::user()->id);

            $user->ezys += $ezys;

            $user->save();

            return 'success';
        }
    }

    public function updatePassword() {
        // Get data from ajax request
        $password                  = Input::get('password');
        $password1                  = Input::get('password1');
        $password2                  = Input::get('password2');

        $errorString = '';
        $errorArray = array();

        if(empty($password)) {
            $errorString .= 'Please fill in all information!<br />';
            array_push($errorArray, 'password');
        }

        if(empty($password1)) {
            $errorString .= 'Please fill in all information!<br />';
            array_push($errorArray, 'password1');
        }

        if(empty($password2)) {
            $errorString .= 'Please fill in all information!<br />';
            array_push($errorArray, 'password2');
        }

        if($password1 != $password2) {
            $errorString .= 'Passwords entered do not match!<br />';
        }

        $user = User::find(Auth::user()->id);

        if(!Hash::check($password, $user->password)) {
            $errorString .= 'Current password entered does not match your current password!<br />';
        }

        if(!empty($errorString)) {
            return json_encode(array('message' => $errorString, 'errorLocation' => $errorArray));
        } else {
            // Update password
            $user->password = Hash::make($password1);

            $user->save();

            return 'success';
        }
    }
}
