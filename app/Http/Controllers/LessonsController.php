<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Auth;

use App\Comment as Comment;
use App\Course as Course;
use App\Lesson as Lesson;
use App\User as User;

use App\Access as Access;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;

class LessonsController extends Controller
{
    // Publicly accessed
    public function index($paginate = 12) {
        $lessons = Lesson::where(['courseID' => NULL, 'privacy' => 1])->orderBy('created_at', 'desc')->paginate($paginate);
        $total_lessons = Lesson::where(['courseID' => NULL, 'privacy' => 1])->orderBy('created_at', 'desc')->get();

        $total_returned = count($lessons);
        $total_lessons = count($total_lessons);

        $place_action = 'archive';

        $userName = '';

        return view('lessons.index', compact(['lessons', 'total_lessons', 'total_returned', 'place_action', 'userName']));
    }

    public function show($id, $paginate = 12) {
        // Find lesson by ID
        $lesson = Lesson::find($id);
        $comments = false;
        $author = false;
        $accessToCourse = false;

        if(!$lesson) {
            $lesson = false;
        } else {
            $comments = Comment::where(['lessonID' => $id])->orderBy('created_at', 'desc')->paginate($paginate);
            $count_comments = Comment::where(['lessonID' => $id])->orderBy('created_at', 'desc')->get();
            $total_comments = count($count_comments);

            $author = User::find($lesson->authorID);

            if(!$author) {
                $author = false;
            } else {
                $author = $author->name;
            }

            $user =  Auth::user();

            if($user) {
                if($lesson->authorID == $user->id OR $user->role == 'moderator' OR $user->role == 'administrator') {
                    $accessToCourse = true;
                }
            }

            if(!$accessToCourse) {
                if($lesson->type == '0') {
                    $accessToCourse = true;
                } else {
                    if($user) {
                        // Access for course, else lesson
                        if($lesson->courseID != null) {
                            $access = Access::where(['courseID' => $lesson->courseID, 'userID' => $user->id])->get();

                            if(count($access) > 0) {
                                $accessToCourse = true;
                            }
                        } else {
                            $access = Access::where(['lessonID' => $lesson->id, 'userID' => $user->id])->get();

                            if(count($access) > 0) {
                                $accessToCourse = true;
                            }
                        }

                        if(Auth::user()->role == 'suspended') {
                            $accessToCourse = false;
                        }
                    }
                }
            }
        }

        return view('lessons.show', compact('lesson', 'author', 'comments', 'total_comments', 'accessToCourse'));
    }

    // Profile access
    public function profileLessons($paginate = 12) {
        if(Auth::user()->role == 'suspended') {
            return view('profile.suspended');
        }

        $user =  Auth::user();

        $lessons = Lesson::where(['authorID' => $user->id])->orderBy('created_at', 'desc')->paginate($paginate);
        $total_lessons = Lesson::where(['authorID' => $user->id])->orderBy('created_at', 'desc')->get();

        $total_returned = count($lessons);
        $total_lessons = count($total_lessons);

        $place_action = 'profile';

        $userName = '';

        return view('lessons.index', compact(['lessons', 'total_lessons', 'total_returned', 'place_action', 'userName']));
    }

    // User lessons
    public function userLessons($userID, $paginate = 12) {
        if(Auth::user()->role == 'suspended') {
            return view('profile.suspended');
        }

        $user =  User::find($userID);

        $lessons = Lesson::where(['authorID' => $user->id])->orderBy('created_at', 'desc')->paginate($paginate);
        $total_lessons = Lesson::where(['authorID' => $user->id])->orderBy('created_at', 'desc')->get();

        $total_returned = count($lessons);
        $total_lessons = count($total_lessons);

        $place_action = 'user';

        $userName = $user->name;

        return view('lessons.index', compact(['lessons', 'total_lessons', 'total_returned', 'place_action', 'userName']));
    }

    public function getProfileLessonCreate() {
        if(Auth::user()->role == 'suspended') {
            return view('profile.suspended');
        }

        $courses = Course::where(['authorID' => Auth::user()->id])->orderBy('created_at', 'desc')->get();

        return view('lessons.create', compact(['courses']));
    }

    public function postProfileLessonCreate() {
        // Get data from ajax request
        $title                  = Input::get('title');
        $excerpt                = Input::get('excerpt');
        $thumbnail              = Input::get('thumbnail');
        $lesson                 = Input::get('lesson');
        $tag                    = Input::get('tag');
        $type                   = Input::get('type');
        $price                  = Input::get('price');
        $lessonpartofcourse     = Input::get('lessonpartofcourse');
        $course                 = Input::get('course');

        $errorString = '';
        $errorArray = array();

        if(empty($title)) {
            $errorString .= 'Please fill in title!<br />';
            array_push($errorArray, 'title');
        }

        if(empty($excerpt)) {
            $errorString .= 'Please fill in excerpt!<br />';
            array_push($errorArray, 'excerpt');
        }

        if(empty($lesson)) {
            $errorString .= 'Please fill in lesson!<br />';
            array_push($errorArray, 'lesson');
        }

        if($type == '1' && empty($price)) {
            $errorString .= 'Price may not be empty if lesson type is paid!<br />';
            array_push($errorArray, 'price');
        }

        if($lessonpartofcourse == '1' && $course == '0') {
            $errorString .= 'Course may not be empty if lesson is part of the course!<br />';
            array_push($errorArray, 'course');
        }

        if(Auth::user()->role == 'suspended') {
            $errorString .= 'You may not complete this action because your account has been suspended!<br />';
        }

        $courseAddTo = Course::find($course);

        if($course != '0' && !$courseAddTo) {
            $errorString .= 'This course does not exist!!<br />';
        }

        if(!empty($errorString)) {
            return json_encode(array('message' => $errorString, 'errorLocation' => $errorArray));
        } else {
            $newLesson = new Lesson;
            $newLesson->title      = $title;
            $newLesson->excerpt    = $excerpt;

            if(empty($thumbnail)) {
                $newLesson->thumbnail = null;
            } else {
                $newLesson->thumbnail = $thumbnail;
            }

            $newLesson->body       = $lesson;

            if(!empty($tag)) {
                $newLesson->tag = $tag;
            }

            if($type == 1 && $course == 0) {

            } else {
                $newLesson->price  = 0;
            }

            if($course != 0) {
                $newLesson->price  = $courseAddTo->price;
                $newLesson->type = $courseAddTo->type;
            } else {
                $newLesson->price  = $price;
                $newLesson->type  = $type;
            }

            $newLesson->privacy    = 0;

            if($lessonpartofcourse == 1 && $course != 0) {
                $newLesson->courseID   = $course;
            } else {
                $newLesson->courseID   = null;
            }

            $newLesson->authorID   = Auth::user()->id;

            $newLesson->save();

            return 'success';
        }
    }

    public function getProfileLessonUpdate($id) {
        $lesson = Lesson::find($id);

        if(Auth::user()->role == 'suspended') {
            return view('profile.suspended');
        } else if(!$lesson) {
            return Redirect::to('profile/lessons');
        } else if($lesson->authorID != Auth::user()->id) {
            return Redirect::to('profile/lessons');
        } else {
            $courses = Course::where(['authorID' => Auth::user()->id])->orderBy('created_at', 'desc')->get();

            return view('lessons.update', compact(['lesson', 'courses']));
        }
    }

    public function postProfileLessonUpdate() {
        // Get data from ajax request
        $lessonID               = Input::get('lessonID');
        $title                  = Input::get('title');
        $excerpt                = Input::get('excerpt');
        $thumbnail              = Input::get('thumbnail');
        $lesson                 = Input::get('lesson');
        $tag                    = Input::get('tag');

        $updateLesson = Lesson::find($lessonID);

        $errorString = '';
        $errorArray = array();

        if($updateLesson->authorID != Auth::user()->id) {
            $errorString .= 'This lesson does not belong to you!<br />';
        } else {
            if(empty($title)) {
                $errorString .= 'Please fill in title!<br />';
                array_push($errorArray, 'title');
            }

            if(empty($excerpt)) {
                $errorString .= 'Please fill in excerpt!<br />';
                array_push($errorArray, 'excerpt');
            }

            if(empty($lesson)) {
                $errorString .= 'Please fill in lesson!<br />';
                array_push($errorArray, 'lesson');
            }
        }

        if(Auth::user()->role == 'suspended') {
            $errorString .= 'You may not complete this action because your account has been suspended!<br />';
        }

        if(!empty($errorString)) {
            return json_encode(array('message' => $errorString, 'errorLocation' => $errorArray));
        } else {
            $updateLesson->title      = $title;
            $updateLesson->excerpt    = $excerpt;

            if(empty($thumbnail)) {
                $updateLesson->thumbnail = null;
            } else {
                $updateLesson->thumbnail = $thumbnail;
            }

            if(!empty($tag)) {
                $updateLesson->tag = $tag;
            }

            $updateLesson->body       = $lesson;

            $updateLesson->save();

            return 'success';
        }
    }

    public function getProfileLessonDelete() {
        $lessonID  = Input::get('lessonID');

        $lesson = Lesson::find($lessonID);

        if(Auth::user()->role == 'suspended') {
            return 'You may not complete this action because your account has been suspended!<br />';
        } else if(!$lesson) {
            return 'An error has occurred while trying to delete lesson.';
        } else if($lesson->authorID != Auth::user()->id) {
            return 'This lesson does not belong to you';
        } else {
            // Delete lesson + its content
            // Lesson's accessess
            $lessonAccesses = Access::where(['lessonID' => $lesson->id])->get();

            foreach($lessonAccesses as $lessonAccess) {
                $lessonAccess->forceDelete();
            }

            // Lesson's comments
            $lessonComments = Comment::where(['lessonID' => $lesson->id])->get();

            foreach($lessonComments as $lessonComment) {
                $lessonComment->forceDelete();
            }

            $lesson->forceDelete();

            return 'success';
        }
    }

    public function buyAccess() {
        if(Auth::user()->role == 'suspended') {
            return 'You may not complete this action because your account has been suspended!<br />';
        }

        $lessonID               = Input::get('lessonID');

        $lesson = Lesson::find($lessonID);
        $user = Auth::user();

        if(!$lesson OR !$user) {
            return 'An error has occurred while buying access to this course.';
        }

        if($lesson->courseID != null) {
            $course = Course::find($lesson->courseID);

            if(!$course) {
                return 'An error has occurred while buying access to this course.';
            } else {
                $access = Access::where(['courseID' => $course->id, 'userID' => $user->id])->get();

                if($course->type == '0' OR $course->authorID == $user->id OR $user->role == 'moderator' OR $user->role == 'administrator' OR count($access) > 0) {
                    return 'You already have access to this lesson!';
                } else if($user->ezys < $course->price) {
                    return 'You do not have enough ezys to pay for the access of this course!';
                } else {
                    $author = User::find($course->authorID);
                    $author->ezys += $course->price;

                    $author->save();

                    $user->ezys -= $course->price;
                    $user->save();

                    $newAccess = new Access;
                    $newAccess->courseID = $course->id;
                    $newAccess->userID = $user->id;

                    $newAccess->save();

                    return 'success';
                }
            }
        } else {
            $access = Access::where(['lessonID' => $lessonID, 'userID' => $user->id])->get();

            if($lesson->type == '0' OR $lesson->authorID == $user->id OR $user->role == 'moderator' OR $user->role == 'administrator' OR count($access) > 0) {
                return 'You already have access to this lesson!';
            } else if($user->ezys < $lesson->price) {
                return 'You do not have enough ezys to pay for the access of this course!';
            } else {
                $author = User::find($lesson->authorID);
                $author->ezys += $lesson->price;

                $author->save();

                $user->ezys -= $lesson->price;
                $user->save();

                $newAccess = new Access;
                $newAccess->lessonID = $lesson->id;
                $newAccess->userID = $user->id;

                $newAccess->save();

                return 'success';
            }
        }
    }
}
