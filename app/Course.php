<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    protected $fillable = [
      'title',
      'excerpt',
      'thumbnail',
      'type',
      'price',
      'privacy'
    ];
}
