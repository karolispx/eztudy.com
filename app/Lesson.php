<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Lesson extends Model
{
    protected $fillable = [
        'title',
        'body',
        'excerpt',
        'thumbnail',
        'type',
        'price',
        'privacy',
        'courseID'
    ];
}
