<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLessonsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lessons', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('authorID');
            $table->integer('courseID')->nullable();
            $table->string('title', 60);
            $table->string('excerpt', 300);
            $table->string('thumbnail', 300)->nullable();
            $table->text('body', 3000);
            $table->text('tag', 30)->nullable();
            $table->integer('type');
            $table->integer('price');
            $table->integer('privacy');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('lessons');
    }
}
