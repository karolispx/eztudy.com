<?php

use Illuminate\Database\Seeder;

class LessonsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach (range(1, 5) as $index) {
            DB::table('lessons')->insert([
                'title' => str_random(10),
                'excerpt' => str_random(10),
                'body' => str_random(1000),
                'authorID' => 1,
                'courseID' => 1,
                'price' => 1,
                'privacy' => 1
            ]);
        }

        foreach (range(1, 15) as $index) {
            DB::table('lessons')->insert([
                'title' => str_random(10),
                'excerpt' => str_random(10),
                'body' => str_random(1000),
                'authorID' => 1,
                'courseID' => NULL,
                'price' => 1,
                'privacy' => 1
            ]);
        }
    }
}
