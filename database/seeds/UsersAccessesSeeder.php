<?php

use Illuminate\Database\Seeder;

class UsersAccessesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
            DB::table('accesses')->insert([
                'userID' => 1,
                'courseID' => 1
            ]);

            DB::table('accesses')->insert([
                'userID' => 1,
                'lessonID' => 1
            ]);
    }
}
