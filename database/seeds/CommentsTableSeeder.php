<?php

use Illuminate\Database\Seeder;

class CommentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach (range(1, 30) as $index) {
            DB::table('comments')->insert([
                'comment' => str_random(40),
                'author' => 'comment author',
                'authorID' => 1,
                'lessonID' => 1
            ]);
        }
    }
}
