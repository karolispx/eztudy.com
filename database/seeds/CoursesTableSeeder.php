<?php

use Illuminate\Database\Seeder;

class CoursesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach (range(1, 10) as $index) {
            DB::table('courses')->insert([
                'title' => str_random(10),
                'excerpt' => str_random(10),
                'authorID' => 1,
                'type' => 1,
                'price' => 1,
                'privacy' => 1
            ]);
        }
    }
}
