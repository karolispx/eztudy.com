var elixir = require('laravel-elixir');
/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {
    // Compiles scss
    mix.sass('app.scss');

    // Concatinates vendor js files into 'vendor.js'
    mix.styles(['jquery-ui-1.11.4.css', 'BootSideMenu.css', 'vendor.css'], 'public/css/vendor.css', 'resources/assets/css');

    //// Concatinates app.css and vendor.css into 'app.css'
    mix.styles(['app.css', 'vendor.css'], 'public/css/app.css', 'public/css');

    //// Concatinates header js files into 'header.js'
    mix.scripts(['jquery-1.12.0.js', 'jquery-ui-1.11.4.js', 'BootSideMenu.js', 'html5shiv.min.js', 'respond.min.js'], 'public/output/header.js','resources/assets/js');

    //// Concatinates footer js files into 'footer.js'
    mix.scripts(['bootstrap-3.3.6.js', 'custom.js', 'lessons.js', 'courses.js', 'notes.js', 'profile.js', 'admin.js'],'public/output/footer.js','resources/assets/js');

    //// Versions footer.js, header.js, app.css
    mix.version(["public/output/footer.js", "public/output/header.js", "public/css/app.css"]);
});