@extends('...layouts.default')
@section('content')

	<div id="content_main">
		<div class="section section-white">
			<div class="container">
                <div class="archive_title">
                    <div class="row">
                      <div class="col-xs-6">
                          @if(($place_action == 'user' AND $userName == $user->name) OR $place_action == 'profile')
                            <h1>My Courses</h1>
                            <a href="{{ url('/profile/course/create') }}" class="button button-default button-tiny button-rounded">Add New</a>
                          @elseif($place_action == 'user')
                              <h1>{{ $userName }}'s Courses</h1>
                          @else
                              <h1>Courses</h1>
                          @endif
                      </div>

                      <div class="col-xs-6">
                        <p class="align_right">Showing <strong>{{ $total_returned }}</strong> of <strong>{{ $total_courses }}</strong> results.</p>
                      </div>
                    </div>
                </div>

                <hr />

                {{ csrf_field() }}

                @if (count($courses) > 0)
                    <div class="row">
                        @foreach ($courses as $course)
                            <div class="col-md-3 col-sm-6 col-xs-6">
                                <div class="archive_box">
                                    <div class="archive_thumbnail">
                                        @if(empty($course->thumbnail))
                                            <a href="{{ url('/course', $course->id) }}"><img class="img-responsive" src="http://placehold.it/350x250" alt=""></a>
                                        @else
                                            <a href="{{ url('/course', $course->id) }}"><img class="img-responsive" src="{{$course->thumbnail}}" alt=""></a>
                                        @endif
                                    </div>

                                    <div class="archive_info">
                                        <h2><a href="{{ url('/course', $course->id) }}">{{ $course->title }}</a></h2>
                                        <p>
                                            @if ($course->type == 1)
                                                <i class="fa fa-money"></i> {{ $course->price }} &#124;
                                            @endif

                                            <i class="fa fa-book"></i> {{ $lessons }}
                                            </p>
                                    </div>

                                    @if($user)
                                        @if($course->authorID == $user->id)
                                            <a title="Edit Course" href="{{ url('/profile/course/update', $course->id) }}"><i class="fa fa-pencil-square-o"></i></a>
                                            <a data-course_delete="{{ $course->id }}" title="Delete Course" class="confirm-delete-course" href="#"><i class="fa fa-remove"></i></a>
                                            @if($course->privacy == 1)
                                                <i title="Public" class="fa fa-eye"></i>
                                            @else
                                                <i title="Private" class="fa fa-eye-slash"></i>
                                            @endif
                                        @endif
                                    @endif
                                </div>
                            </div>
                        @endforeach
                    </div>

                    <hr />
                    <div class="align_center">
                        {{ $courses->links() }}
                    </div>
                @else
                    <p>No Courses have been found!</p>
                @endif

			</div>
		</div>
	</div>
@stop