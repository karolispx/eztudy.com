@extends('...layouts.default')
@section('content')

    <br />

	<div id="content_main">
		<div class="section section-white">
            <div class="container">

                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        <div class="returnedMessage"></div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">Create New Course</div>
                            <div class="panel-body">
                                <form class="form-horizontal" role="form" method="POST" action="{{ url('/profile/course/create') }}" id="create-course-form" name="create-lesson-form">

                                    {{ csrf_field() }}

                                    <div class="form-group">
                                        <label class="col-md-3 control-label" for="title">Course Title</label>

                                        <div class="col-md-8">
                                            <input type="text" class="form-control required" maxlength="60" id="title" name="title">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-3 control-label" for="excerpt">Course Excerpt</label>

                                        <div class="col-md-8">
                                            <textarea class="form-control required" maxlength="300" id="excerpt" name="excerpt"></textarea>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-3 control-label" for="thumbnail">Course Thumbnail URL(with http://)</label>

                                        <div class="col-md-8">
                                            <input type="text" class="form-control" maxlength="255" id="thumbnail" name="thumbnail">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-3 control-label" for="tag">Course Tag</label>

                                        <div class="col-md-8">
                                            <input type="text" class="form-control" maxlength="30" id="tag" name="tag">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Course Type</label>

                                        <div class="col-md-8">
                                            <input type="radio" name="type" value="0" checked="checked"> Free <input type="radio" name="type" value="1"> Paid<br>
                                        </div>
                                    </div>

                                    <div id="price_section" class="form-group">
                                        <label class="col-md-3 control-label" for="price">Course Price</label>

                                        <div class="col-md-8">
                                            <input type="number" class="form-control" size="10" id="price" name="price">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-md-8 col-md-offset-3">
                                            <button id="create-course-button" type="submit" class="btn btn-primary">
                                                <i class="fa fa-btn fa-plus"></i> Create Course
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop