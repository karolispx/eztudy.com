@extends('...layouts.default')
@section('content')
	<div id="content_main">
		<div class="section section-white">
			<div class="container">
                @if($course)
                    <h1>{{ $course->title }}</h1>

                    @if(isset($user) && $user->name == $author)
                        @if($user->role != 'moderator' && $user->role != 'administrator')
                            <a href="{{ url('/profile/course/update', $course->id) }}" class="button button-default button-tiny button-rounded">Manage Course</a>
                        @endif
                    @endif

                     @if($user)
                         @if($user->role == 'moderator' OR $user->role == 'administrator')
                             <a href="{{ url('/admin/course/manage', $course->id) }}" class="button button-default button-tiny button-rounded">Moderate Course</a>
                         @endif
                     @endif

                    <p class="underline">
                        <i class="fa fa-calendar"></i> {{ $course->created_at }} &#124; <i class="fa fa-user"></i> {{ $author }}
                    </p>

                    <hr />

                    <p>{{ $course->excerpt }}</p>

                    <hr />

                    @if($accessToCourse)
                        @if (count($lessons) > 0)
                            <h2>Lessons</h2>
                            <div class="row">
                                @foreach ($lessons as $lesson)
                                   <div class="col-md-3 col-sm-6 col-xs-6">
                                       <div class="archive_box">
                                           <div class="archive_thumbnail">
                                               <a href="{{ url('/lesson', $lesson->id) }}"><img class="img-responsive" src="http://placehold.it/350x250" alt=""></a>
                                           </div>

                                           <div class="archive_info">
                                               <h2><a href="{{ url('/lesson', $lesson->id) }}">{{ $lesson->title }}</a></h2>
                                               <p><i class="fa fa-money"></i> 3 &#124; <i class="fa fa-book"></i> 4</p>
                                           </div>
                                       </div>
                                   </div>
                                @endforeach
                            </div>
                        @else
                            <p>No Lessons have been found!</p>
                        @endif

                        @if (!empty($course->tag))
                            <hr />

                            <p class="underline">
                                <i class="fa fa-tag"></i> <a href="{{ url('/search') }}">{{ $course->tag }}</a>
                            </p>
                        @endif
                    @else
                        @if($user)
                            <h1 class="align_center" style="color: red;">Premium content!</h1>
                            <h3 class="align_center">You need access to this course to view its lessons!</h3>
                            <h4 class="align_center">
                                {{ csrf_field() }}

                                <a title="Buy access to the course" data-course_access="{{ $course->id }}" href="#" class="button button-default button-tiny button-rounded confirm-buy-access-course">Buy Access ({{ $course->price }}) ezys</a>
                            </h4>
                        @else
                            <h1 class="align_center" style="color: red;">Premium content!</h1>
                            <h3 class="align_center">You need to be signed in and have access to this course to view its lessons!</h3>
                            <h4 class="align_center"><a href="{{ url('/login') }}"><i class="fa fa-sign-in"></i> Sign in</a></h4>
                        @endif
                    @endif
                @else
                    <h1 class="align_center">This course does not exists!</h1>
               @endif
			</div>
		</div>
	</div>
@stop