@extends('...layouts.default')
@section('content')

    <br />

	<div id="content_main">
		<div class="section section-white">
            <div class="container">

                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        <div class="returnedMessage"></div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-default">
                                <div class="panel-heading">Update Course: <a href="{{ url('/course', $course->id) }}">{{ $course->title }}</a></div>
                            <div class="panel-body">
                                <form class="form-horizontal" role="form" method="POST" action="{{ url('/profile/course/update') }}" id="update-course-form" name="update-course-form">

                                    {{ csrf_field() }}

                                    <input type="hidden" id="courseID" name="courseID" value="{{ $course->id }}">

                                    <div class="form-group">
                                        <label class="col-md-3 control-label" for="title">Course Title</label>

                                        <div class="col-md-8">
                                            <input type="text" class="form-control required" maxlength="60" id="title" name="title" value="{{ $course->title }}">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-3 control-label" for="excerpt">Course Excerpt</label>

                                        <div class="col-md-8">
                                            <textarea class="form-control required" maxlength="300" id="excerpt" name="excerpt">{{ $course->excerpt }}</textarea>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-3 control-label" for="thumbnail">Course Thumbnail URL(with http://)</label>

                                        <div class="col-md-8">
                                            <input type="text" class="form-control" maxlength="255" id="thumbnail" name="thumbnail" value="{{ $course->thumbnail }}">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-3 control-label" for="tag">Course Tag</label>

                                        <div class="col-md-8">
                                            <input type="text" class="form-control" maxlength="30" id="tag" name="tag" value="{{ $course->tag }}">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Course Type</label>

                                        <div class="col-md-8">
                                            @if($course->type == '1')
                                                <input type="radio" name="type" value="0" disabled> Free <input type="radio" name="type" value="1" checked="checked" disabled> Paid<br>
                                            @else
                                                <input type="radio" name="type" value="0" checked="checked" disabled> Free <input type="radio" name="type" value="1" disabled> Paid<br>
                                            @endif
                                        </div>

                                        <div class="col-md-8">
                                        </div>
                                    </div>

                                    <div id="price_section" class="form-group">
                                        <label class="col-md-3 control-label" for="price">Course Price</label>

                                        <div class="col-md-8">
                                            <input type="number" class="form-control" size="10" id="price" name="price" value="{{ $course->price }}" disabled>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-md-8 col-md-offset-3">
                                            <button id="update-course-button" type="submit" class="btn btn-primary">
                                                <i class="fa fa-btn fa-floppy-o"></i> Update Course
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

                @if (count($lessons) > 0)
                    <h2>Course's Lessons</h2>
                    <div class="row">
                        @foreach ($lessons as $lesson)
                           <div class="col-md-3 col-sm-6 col-xs-6">
                               <div class="archive_box">
                                   <div class="archive_thumbnail">
                                       <a href="{{ url('/lesson', $lesson->id) }}"><img class="img-responsive" src="http://placehold.it/350x250" alt=""></a>
                                   </div>

                                   <div class="archive_info">
                                       <h2><a href="{{ url('/lesson', $lesson->id) }}">{{ $lesson->title }}</a></h2>
                                       <p><i class="fa fa-money"></i> 3 &#124; <i class="fa fa-book"></i> 4</p>
                                   </div>
                                   <a data-lesson_remove="{{ $lesson->id }}" data-course_remove="{{ $course->id }}" title="Remove Lesson From This Course" class="confirm-remove-lesson" href="#"><i class="fa fa-remove"></i></a>
                               </div>
                           </div>
                        @endforeach
                    </div>
                @else
                    <p>No Lessons have been found!</p>
                @endif
            </div>
        </div>
    </div>
@stop