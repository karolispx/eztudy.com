@extends('...layouts.default')
@section('content')

    <br />

	<div id="content_main">
		<div class="section section-white">
            <div class="container">

                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        <div class="returnedMessage"></div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">Create New Note</div>
                            <div class="panel-body">
                                <form class="form-horizontal" role="form" method="POST" action="{{ url('/profile/note/create') }}" id="create-note-form" name="create-note-form">

                                    {{ csrf_field() }}

                                    <div class="form-group">
                                        <label class="col-md-3 control-label" for="note">Note</label>

                                        <div class="col-md-8">
                                            <textarea class="form-control required" maxlength="300" id="note" name="note"></textarea>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Note Privacy</label>

                                        <div class="col-md-8">
                                            <input type="radio" name="privacy" value="0" checked="checked"> Private <input type="radio" name="privacy" value="1"> Public<br>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-md-8 col-md-offset-3">
                                            <button id="create-note-button" type="submit" class="btn btn-primary">
                                                <i class="fa fa-btn fa-plus"></i> Create Note
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop