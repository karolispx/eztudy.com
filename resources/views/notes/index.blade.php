@extends('...layouts.default')
@section('content')

	<div id="content_main">
		<div class="section section-white">
			<div class="container">
                <div class="archive_title">
                    <div class="row">
                      <div class="col-xs-6">
                        @if(($place_action == 'user' AND $userName == $user->name) OR $place_action == 'profile')
                            <h1>My Notes</h1>
                            <a href="{{ url('/profile/note/create') }}" class="button button-default button-tiny button-rounded">Add New</a>
                        @else
                            <h1>{{ $userName }}'s Public Notes</h1>
                        @endif
                      </div>

                      <div class="col-xs-6">
                        <p class="align_right">Showing <strong>{{ $total_returned }}</strong> of <strong>{{ $total_notes }}</strong> results.</p>
                      </div>
                    </div>
                </div>

                <hr />

                {{ csrf_field() }}

                @if (count($notes) > 0)
                    <div class="row">
                        @foreach ($notes as $note)
                            <div class="col-md-12">
                                <div class="archive_box">
                                    <div class="archive_info">
                                        <p>{{ $note->note }}<br />
                                            <small>[{{ $note->created_at }}]</small>
                                        </p>
                                    </div>

                                    @if($user)
                                        @if($note->authorID == $user->id)
                                            <a title="Edit Note" href="{{ url('/profile/note/update', $note->id) }}"><i class="fa fa-pencil-square-o"></i></a>
                                            <a data-note_delete="{{ $note->id }}" title="Delete Note" class="confirm-delete-note" href="#"><i class="fa fa-remove"></i></a>

                                            @if(!empty($note->lessonID))
                                                <a title="Posted in" href="{{ url('/lesson', $note->lessonID) }}"><i class="fa fa-book"></i></a>
                                            @endif

                                            @if($note->privacy == 1)
                                                <i title="Public" class="fa fa-eye"></i>
                                            @else
                                                <i title="Private" class="fa fa-eye-slash"></i>
                                            @endif
                                        @endif
                                    @endif

                                    @if($user->role == 'administrator' && $note->authorID != $user->id)
                                        <a href="{{ url('/admin/note/manage', $note->id) }}" class="button button-default button-tiny button-rounded">Moderate Note</a>
                                    @endif
                                </div>
                            </div>
                        @endforeach
                    </div>

                    <hr />
                    <div class="align_center">
                        {{ $notes->links() }}
                    </div>
                @else
                    <p>No Notes have been found!</p>
                @endif
			</div>
		</div>
	</div>
@stop