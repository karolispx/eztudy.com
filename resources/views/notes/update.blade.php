@extends('...layouts.default')
@section('content')

    <br />

	<div id="content_main">
		<div class="section section-white">
            <div class="container">

                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        <div class="returnedMessage"></div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">Update Note</div>
                            <div class="panel-body">
                                <form class="form-horizontal" role="form" method="POST" action="{{ url('/profile/note/update') }}" id="update-note-form" name="update-note-form">

                                    {{ csrf_field() }}

                                    <input type="hidden" id="noteID" name="noteID" value="{{ $note->id }}">

                                    <div class="form-group">
                                        <label class="col-md-3 control-label" for="note">Note</label>

                                        <div class="col-md-8">
                                            <input type="text" class="form-control required" maxlength="60" id="note" name="note" value="{{ $note->note }}">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Note Privacy</label>

                                        <div class="col-md-8">
                                            @if($note->privacy == '1')
                                                <input type="radio" name="privacy" value="0"> Private <input type="radio" name="privacy" value="1" checked="checked"> Public<br>
                                            @else
                                                <input type="radio" name="privacy" value="0" checked="checked"> Private <input type="radio" name="privacy" value="1"> Public<br>
                                            @endif
                                        </div>

                                        <div class="col-md-8">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-md-8 col-md-offset-3">
                                            <button id="update-note-button" type="submit" class="btn btn-primary">
                                                <i class="fa fa-btn fa-floppy-o"></i> Update Note
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop