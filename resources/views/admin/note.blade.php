@extends('...layouts.default')
@section('content')

    <br />

	<div id="content_main">
		<div class="section section-white">
            <div class="container">
                @if($note)
                    <div class="row">
                        <div class="col-md-8 col-md-offset-2">
                             <div class="returnedMessage5"></div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-default">

                            <div class="panel-heading">
                                Manage Note
                                @if($user->role == 'administrator')
                                    [<a data-note_delete="{{ $note->id }}" href="#" class="confirm-admin-note-delete">Delete Note</a>]
                                @endif
                            </div>

                            <input type="hidden" id="noteID" name="noteID" value="{{ $note->id }}">

                            <div class="panel-body">
                                <form class="form-horizontal" role="form" method="POST" action="{{ url('/admin/note/manage') }}" id="admin-manage-note-form" name="admin-manage-note-form">
                                    {{ csrf_field() }}

                                    <div class="form-group">
                                        <label class="col-md-3 control-label" for="note">Note</label>

                                        <div class="col-md-8">
                                            <textarea class="form-control required" maxlength="700" id="note" name="note">{{ $note->note }}</textarea>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-3 control-label" for="privacy">Privacy</label>

                                        <div class="col-md-8">
                                            <select id="privacy">
                                                @if($note->privacy == '1')
                                                    <option value="0">Private</option>
                                                    <option value="1" selected="selected">Public</option>
                                                @else
                                                    <option value="0" selected="selected">Private</option>
                                                    <option value="1">Public</option>
                                                @endif
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-md-8 col-md-offset-3">
                                            <button id="admin-manage-note-button" type="submit" class="btn btn-primary">
                                                 <i class="fa fa-btn fa-save"></i> Update Note
                                            </button>
                                        </div>
                                    </div>
                                </form>
                                </div>
                            </div>
                        </div>
                    </div>
                @else
                    <h1 class="align_center">This note does not exists!</h1>
                @endif
            </div>
        </div>
    </div>

@stop