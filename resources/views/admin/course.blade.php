@extends('...layouts.default')
@section('content')

    <br />

	<div id="content_main">
		<div class="section section-white">
            <div class="container">
                @if($course)
                    <div class="row">
                        <div class="col-md-8 col-md-offset-2">
                             <div class="returnedMessage5"></div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-default">

                            <div class="panel-heading">
                                Manage Course: <a href="{{ url('/course', $course->id) }}">{{ $course->title }}</a>
                                @if($user->role == 'administrator')
                                    [<a data-course_delete="{{ $course->id }}" href="#" class="confirm-admin-course-delete">Delete Course</a>]
                                @endif
                            </div>

                            <div class="panel-body">
                                <form class="form-horizontal" role="form" method="POST" action="{{ url('/admin/course/manage') }}" id="admin-manage-course-form" name="admin-manage-course-form">
                                    {{ csrf_field() }}

                                    <input type="hidden" id="courseID" name="courseID" value="{{ $course->id }}">

                                    <div class="form-group">
                                        <label class="col-md-3 control-label" for="title">Course Title</label>

                                        <div class="col-md-8">
                                            <input type="text" class="form-control required" maxlength="60" id="title" name="title" value="{{ $course->title }}">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-3 control-label" for="excerpt">Course Excerpt</label>

                                        <div class="col-md-8">
                                            <textarea class="form-control" maxlength="300" id="excerpt" name="excerpt">{{ $course->excerpt }}</textarea>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-3 control-label" for="thumbnail">Course Thumbnail URL(with http://)</label>

                                        <div class="col-md-8">
                                            <input type="text" class="form-control required" maxlength="255" id="thumbnail" name="thumbnail" value="{{ $course->thumbnail }}">
                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <label class="col-md-3 control-label" for="tag">Course Tag</label>

                                        <div class="col-md-8">
                                            <input type="text" class="form-control" maxlength="30" id="tag" name="tag" value="{{ $course->tag }}">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-3 control-label" for="privacy">Privacy</label>

                                        <div class="col-md-8">
                                            <select id="privacy">
                                                @if($course->privacy == '1')
                                                    <option value="0">Private</option>
                                                    <option value="1" selected="selected">Public</option>
                                                @else
                                                    <option value="0" selected="selected">Private</option>
                                                    <option value="1">Public</option>
                                                @endif
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Course Type</label>

                                        <div class="col-md-8">
                                            @if($course->type == '1')
                                                <input type="radio" name="type" value="0" disabled> Free <input type="radio" name="type" value="1" checked="checked" disabled> Paid<br>
                                            @else
                                                <input type="radio" name="type" value="0" checked="checked" disabled> Free <input type="radio" name="type" value="1" disabled> Paid<br>
                                            @endif
                                        </div>

                                        <div class="col-md-8">
                                        </div>
                                    </div>

                                    <div id="price_section" class="form-group">
                                        <label class="col-md-3 control-label" for="price">Course Price</label>

                                        <div class="col-md-8">
                                            <input type="number" class="form-control" size="10" id="price" name="price" value="{{ $course->price }}" disabled>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-md-8 col-md-offset-3">
                                            <button id="admin-manage-course-button" type="submit" class="btn btn-primary">
                                                 <i class="fa fa-btn fa-save"></i> Update Course
                                            </button>
                                        </div>
                                    </div>
                                </form>
                                </div>
                            </div>
                        </div>
                    </div>
                @else
                    <h1 class="align_center">This course does not exists!</h1>
                @endif
            </div>
        </div>
    </div>

@stop