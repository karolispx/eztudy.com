@extends('...layouts.default')
@section('content')

    <br />

	<div id="content_main">
		<div class="section section-white">
            <div class="container">
                @if($userProfile)
                    @if($userProfile->role != 'administrator')
                        @if($userProfile->role == 'moderator' && $user->role != 'administrator')
                            <h1 class="align_center">You may not manage another moderator!</h1>
                        @else
                            <div class="row">
                                <div class="col-md-8 col-md-offset-2">
                                     <div class="returnedMessage5"></div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="panel panel-default">

                                    <div class="panel-heading">
                                        Manage User: <a href="{{ url('/user', $userProfile->id) }}">{{ $userProfile->name }}</a>
                                        @if($user->role == 'administrator')
                                            [<a data-user_delete="{{ $userProfile->id }}" href="#" class="confirm-admin-user-delete">Delete User</a>]
                                        @endif
                                    </div>

                                    <div class="panel-body">
                                        <form class="form-horizontal" role="form" method="POST" action="{{ url('/admin/user/manage') }}" id="admin-manage-user-form" name="admin-manage-user-form">
                                            {{ csrf_field() }}

                                            <input type="hidden" id="userID" name="userID" value="{{ $userProfile->id }}">

                                            <div class="form-group">
                                                <label class="col-md-3 control-label" for="ezys">Ezys amount</label>

                                                <div class="col-md-8">
                                                    <input type="number" class="form-control required" maxlength="60" id="ezys" name="ezys" value="{{ $userProfile->ezys }}">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-3 control-label" for="role">Role</label>

                                                <div class="col-md-8">
                                                    <select id="role">
                                                        @if($user->role == 'administrator')
                                                             <option value="administrator" selected>Administrator</option>
                                                        @endif

                                                        @if($user->role == 'administrator')
                                                            @if($userProfile->role == 'moderator')
                                                                <option value="moderator" selected="selected">Moderator</option>
                                                            @else
                                                                <option value="moderator">Moderator</option>
                                                            @endif
                                                        @endif

                                                        @if($userProfile->role == 'suspended')
                                                            <option value="suspended" selected="selected">Suspended User</option>
                                                        @else
                                                            <option value="suspended">Suspended User</option>
                                                        @endif

                                                        @if($userProfile->role == 'user')
                                                            <option value="user" selected="selected">Normal User</option>
                                                        @else
                                                            <option value="user">Normal User</option>
                                                        @endif
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="col-md-8 col-md-offset-3">
                                                    <button id="admin-manage-user-button" type="submit" class="btn btn-primary">
                                                         <i class="fa fa-btn fa-save"></i> Update User
                                                    </button>
                                                </div>
                                            </div>
                                        </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endif
                    @else
                        <h1 class="align_center">You may not manage another administrator!</h1>
                    @endif
                @else
                    <h1 class="align_center">This user does not exists!</h1>
                @endif
            </div>
        </div>
    </div>

@stop