@extends('...layouts.default')
@section('content')

    <br />

	<div id="content_main">
		<div class="section section-white">
            <div class="container">

                @if($user->role != 'suspended')
                    <div class="row">
                        <div class="col-md-8 col-md-offset-2">
                            <div class="returnedMessage1"></div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">Buy Ezys</div>
                                <div class="panel-body">
                                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/profile/ezys') }}" id="buy-ezys-form" name="buy-ezys-form">

                                        {{ csrf_field() }}

                                        <div class="form-group">
                                            <label class="col-md-3 control-label" for="ezysamount">Current ezys amount</label>

                                            <div class="col-md-8">
                                                {{ $user->ezys }}
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-md-3 control-label" for="ezys">Buy ezys amount</label>

                                            <div class="col-md-8">
                                                <input type="number" class="form-control required" maxlength="60" id="ezys" name="ezys">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-md-8 col-md-offset-3">
                                                <button id="buy-ezys-button" type="submit" class="btn btn-primary">
                                                    <i class="fa fa-btn fa-money"></i> Buy Ezys
                                                </button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif

                <br />
                <br />

                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        <div class="returnedMessage2"></div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">Update your password</div>
                            <div class="panel-body">
                                <form class="form-horizontal" role="form" method="POST" action="{{ url('/profile/password') }}" id="update-password-form" name="update-password-form">

                                    {{ csrf_field() }}

                                    <div class="form-group">
                                        <label class="col-md-3 control-label" for="password">Current Password</label>

                                        <div class="col-md-8">
                                            <input type="password" class="form-control required" maxlength="60" id="password" name="password">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-3 control-label" for="password1">New Password</label>

                                        <div class="col-md-8">
                                            <input type="password" class="form-control required" maxlength="60" id="password1" name="password1">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-3 control-label" for="password2">New Password</label>

                                        <div class="col-md-8">
                                            <input type="password" class="form-control required" maxlength="60" id="password2" name="password2">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-md-8 col-md-offset-3">
                                            <button id="update-password-button" type="submit" class="btn btn-primary">
                                                <i class="fa fa-btn fa-save"></i> Update Password
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop