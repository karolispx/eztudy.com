@extends('...layouts.default')
@section('content')

	<div id="content_main">
		<div class="section section-white">
            <div class="container">
                <h1 class="align_center" style="color: red;">Limited access!</h1>
                <h3 class="align_center">You may not complete this action because you do not have access!</h3>
            </div>
        </div>
    </div>

@stop