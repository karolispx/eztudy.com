<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <link rel="shortcut icon" type="image/ico" href="/build/images/favicon.ico"/>
    <title>Eztudy - Where study happens</title>

    <!-- app.css - main site css -->
    <link href="{{elixir('css/app.css')}}" rel="stylesheet">
</head>

<body>
	<div id="header">
        <nav class="navbar navbar-default navbar-fixed-top">
          <div class="container">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand" href="{{ url('/') }}">eZtudy</a>
            </div>

            <div id="navbar" class="navbar-collapse collapse">
              <ul class="nav navbar-nav">
                <li {{ Request::is('lessons') ? ' class=active' : '' }}><a href="{{ url('/lessons') }}"><i class="fa fa-book"></i> Lessons</a></li>
                <li {{ Request::is('courses') ? ' class=active' : '' }}><a href="{{ url('/courses') }}"><i class="fa fa-folder-open"></i> Courses</a></li>
              </ul>

              <ul class="nav navbar-nav navbar-right">
                <li>
                  <form class="navbar-form" role="search" action="{{ url('/search') }}">
                      <div class="input-group">
                          <input type="text" class="form-control" placeholder="Search">
                          <div class="input-group-btn">
                              <button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i></button>
                          </div>
                      </div>
                  </form>
                </li>
                @if(isset($user))
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-user"></i> {{ $user->name }}<span class="caret"></span></a>

                        <ul class="dropdown-menu">
                          <li><a href="{{ url('/profile/lessons') }}"><i class="fa fa-book"></i> My Lessons</a></li>
                          <li><a href="{{ url('/profile/courses') }}"><i class="fa fa-folder-open"></i> My Courses</a></li>
                          <li><a href="{{ url('/profile/notes') }}"><i class="fa fa-sticky-note"></i> My Notes</a></li>
                          <li><a href="{{ url('/profile') }}"><i class="fa fa-book"></i> Settings</a></li>
                          <li><a href="{{ url('/logout') }}"><i class="fa fa-sign-out"></i> Logout</a></li>
                        </ul>
                    </li>
                @else
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Join<span class="caret"></span></a>

                        <ul class="dropdown-menu">
                          <li><a href="{{ url('/login') }}"><i class="fa fa-sign-in"></i> Sign In</a></li>
                          <li><a href="{{ url('/register') }}"><i class="fa fa-user-plus"></i> Sign Up</a></li>
                        </ul>
                    </li>
                @endif
              </ul>
            </div>
          </div>
        </nav>
    </div>