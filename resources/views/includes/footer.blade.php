@if(isset($user) AND $user->role != 'suspended')
    <div id="notes-slideout">
        <div class="notes-container">
               <h3>Create New Note</h3>

                <div class="row">
                    <div class="col-md-12">
                        <div class="returnedMessage7"></div>
                    </div>
                </div>

                <form class="form-horizontal" role="form" method="POST" action="{{ url('/profile/note/create') }}" id="create-note-form2" name="create-note-form2">

                    {{ csrf_field() }}

                    @if(!empty($lesson))
                        <input type="hidden" name="lessonID" value="{{ $lesson->id }}">
                    @else
                        <input type="hidden" name="lessonID" value="">
                    @endif

                    <div class="form-group">
                        <label class="col-md-3 control-label" for="note2">Note</label>

                        <div class="col-md-8">
                            <textarea class="form-control" maxlength="300" id="note2" name="note"></textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label">Privacy</label>

                        <div class="col-md-8">
                            <input type="radio" name="privacy2" value="0" checked="checked"> Private <input type="radio" name="privacy2" value="1"> Public<br>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-8 col-md-offset-3">
                            <button id="create-note-button2" type="submit" class="btn btn-primary">
                                <i class="fa fa-btn fa-plus"></i> Create Note
                            </button>
                        </div>
                    </div>
                </form>
        </div>
    </div>
@endif

<div id="footer">
    <div class="container">
        &#169; 2016 eZtudy. All rights reserved.
    </div>
</div>

<!-- header.js - header js (jquery, jquery ui ) --->
<script src="{{elixir('output/header.js')}}"></script>

<!-- footer.js - footer js (custom.js, bootstrap.js ... ) --->
<script src="{{elixir('output/footer.js')}}"></script>

</body>
</html>