@extends('...layouts.default')
@section('content')

    <br />

	<div id="content_main">
		<div class="section section-white">
            <div class="container">

                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        <div class="returnedMessage"></div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">Update Lesson: <a href="{{ url('/lesson', $lesson->id) }}">{{ $lesson->title }}</a></div>

                            <div class="panel-body">
                                <form class="form-horizontal" role="form" method="POST" action="{{ url('/profile/lesson/update') }}" id="update-lesson-form" name="update-lesson-form">

                                    {{ csrf_field() }}

                                    <input type="hidden" id="lessonID" name="lessonID" value="{{ $lesson->id }}">

                                    <div class="form-group">
                                        <label class="col-md-3 control-label" for="title">Lesson Title</label>

                                        <div class="col-md-8">
                                            <input type="text" class="form-control required" maxlength="60" id="title" name="title" value="{{ $lesson->title }}">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-3 control-label" for="excerpt">Lesson Excerpt</label>

                                        <div class="col-md-8">
                                            <textarea class="form-control required" maxlength="300" id="excerpt" name="excerpt">{{ $lesson->excerpt }}</textarea>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-3 control-label" for="thumbnail">Lesson Thumbnail URL(with http://)</label>

                                        <div class="col-md-8">
                                            <input type="text" class="form-control" maxlength="255" id="thumbnail" name="thumbnail" value="{{ $lesson->thumbnail }}">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-3 control-label" for="lesson">Lesson</label>

                                        <div class="col-md-8">
                                            <textarea class="form-control required" maxlength="3000" id="lesson" name="lesson">{{ $lesson->body }}</textarea>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-3 control-label" for="tag">Lesson Tag</label>

                                        <div class="col-md-8">
                                            <input type="text" class="form-control" maxlength="30" id="tag" name="tag" value="{{ $lesson->tag }}">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Lesson Type</label>

                                        <div class="col-md-8">
                                            @if($lesson->type == '1')
                                                <input type="radio" name="type" value="0" disabled> Free <input type="radio" name="type" value="1" checked="checked" disabled> Paid<br>
                                            @else
                                                <input type="radio" name="type" value="0" checked="checked" disabled> Free <input type="radio" name="type" value="1" disabled> Paid<br>
                                            @endif
                                        </div>

                                        <div class="col-md-8">
                                        </div>
                                    </div>

                                    <div id="price_section" class="form-group">
                                        <label class="col-md-3 control-label" for="price">Lesson Price</label>

                                        <div class="col-md-8">
                                            <input type="number" class="form-control" size="10" id="price" name="price" value="{{ $lesson->price }}" disabled>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Part of course?</label>

                                        <div class="col-md-8">
                                            @if($lesson->courseID != null)
                                                <input type="radio" name="lessonpartofcourse" value="0" disabled> No <input type="radio" name="lessonpartofcourse" value="1" checked="checked" disabled> Yes<br>
                                            @else
                                                <input type="radio" name="lessonpartofcourse" value="0" checked="checked" disabled> No <input type="radio" name="lessonpartofcourse" value="1" disabled> Yes<br>
                                            @endif
                                        </div>
                                    </div>

                                    <div id="lesson_course_section" class="form-group">
                                        <label class="col-md-3 control-label" for="course">Course</label>

                                        <div class="col-md-8">
                                            <select id="course" name="course" disabled>
                                                <option value="0">Please select a course</option>
                                                @if (count($courses) > 0)
                                                     @foreach ($courses as $course)
                                                        @if($course->id == $lesson->courseID)
                                                            <option value="{{ $course->id }}" selected="selected">{{ $course->title }}</option>
                                                        @else
                                                            <option value="{{ $course->id }}">{{ $course->title }}</option>
                                                        @endif
                                                     @endforeach
                                                 @endif
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-md-8 col-md-offset-3">
                                            <button id="update-lesson-button" type="submit" class="btn btn-primary">
                                                <i class="fa fa-btn fa-floppy-o"></i> Update Lesson
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop