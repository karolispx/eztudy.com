@extends('...layouts.default')
@section('content')

	<div id="content_main">
		<div class="section section-white">
			<div class="container">
                <div class="archive_title">
                    <div class="row">
                      <div class="col-xs-6">
                          @if(($place_action == 'user' AND $userName == $user->name) OR $place_action == 'profile')
                              <h1>My Lessons</h1>
                                <a href="{{ url('/profile/lesson/create') }}" class="button button-default button-tiny button-rounded">Add New</a>
                          @elseif($place_action == 'user')
                              <h1>{{ $userName }}'s Lessons</h1>
                          @else
                              <h1>Lessons</h1>
                          @endif
                      </div>

                      <div class="col-xs-6">
                        <p class="align_right">Showing <strong>{{ $total_returned }}</strong> of <strong>{{ $total_lessons }}</strong> results.</p>
                      </div>
                    </div>
                </div>

                <hr />

                {{ csrf_field() }}

                @if (count($lessons) > 0)
                    <div class="row">
                        @foreach ($lessons as $lesson)
                            <div class="col-md-3 col-sm-6 col-xs-6">
                                <div class="archive_box">
                                    <div class="archive_thumbnail">
                                        @if(empty($lesson->thumbnail))
                                            <a href="{{ url('/lesson', $lesson->id) }}"><img class="img-responsive" src="http://placehold.it/350x250" alt=""></a>
                                        @else
                                            <a href="{{ url('/lesson', $lesson->id) }}"><img class="img-responsive" src="{{$lesson->thumbnail}}" alt=""></a>
                                        @endif
                                    </div>

                                    <div class="archive_info">
                                        <h2><a href="{{ url('/lesson', $lesson->id) }}">{{ $lesson->title }}</a></h2>
                                        @if($lesson->type == 1)
                                            <p><i class="fa fa-money"></i> {{ $lesson->price }}</p>
                                        @endif
                                    </div>

                                    @if($user)
                                        @if($lesson->authorID == $user->id)
                                            <a title="Edit Lesson" href="{{ url('/profile/lesson/update', $lesson->id) }}"><i class="fa fa-pencil-square-o"></i></a>
                                            <a data-lesson_delete="{{ $lesson->id }}" title="Delete Lesson" class="confirm-delete-lesson" href="#"><i class="fa fa-remove"></i></a>

                                            @if($lesson->courseID != null)
                                                <a title="Course Lesson" href="{{ url('/course', $lesson->courseID) }}"><i class="fa fa fa-folder-open"></i></a>
                                            @endif

                                            @if($lesson->privacy == 1)
                                                <i title="Public" class="fa fa-eye"></i>
                                            @else
                                                <i title="Private" class="fa fa-eye-slash"></i>
                                            @endif
                                        @endif
                                    @endif
                                </div>
                            </div>
                        @endforeach
                    </div>

                    <hr />
                    <div class="align_center">
                        {{ $lessons->links() }}
                    </div>
                @else
                    <p>No Lessons have been found!</p>
                @endif
			</div>
		</div>
	</div>
@stop