@extends('...layouts.default')
@section('content')
	<div id="content_main">
		<div class="section section-white">
			<div class="container">
                @if($lesson)
                    <h1>{{ $lesson->title }}</h1>
                    @if(isset($user) && $user->name == $author)
                        @if($user->role != 'moderator' && $user->role != 'administrator')
                            <a href="{{ url('/profile/lesson/update', $lesson->id) }}" class="button button-default button-tiny button-rounded">Manage Lesson</a>
                        @endif
                    @endif

                     @if($user)
                         @if($user->role == 'moderator' OR $user->role == 'administrator')
                             <a href="{{ url('/admin/lesson/manage', $lesson->id) }}" class="button button-default button-tiny button-rounded">Moderate Lesson</a>
                         @endif
                     @endif

                    <p class="underline">
                        <i class="fa fa-calendar"></i> {{ $lesson->created_at }}

                         @if ($author)
                             &#124; <i class="fa fa-user"></i> {{ $author }}
                         @endif
                    </p>
                    <hr />

                    @if($accessToCourse)
                        {{ $lesson->body }}

                        @if (!empty($lesson->tag))
                            <hr />

                            <p class="underline">
                                <i class="fa fa-tag"></i> <a href="{{ url('/search') }}">{{ $lesson->tag }}</a>
                            </p>
                        @endif

                        @if(isset($user))
                            <hr />
                                <h4><i class="fa fa-comment-o"></i> Comments {{ $total_comments }}</h4>

                                @if ($comments && count($comments) > 0)
                                    @foreach ($comments as $comment)
                                        <p>
                                            <strong><a href="{{ url('/user', $comment->authorID) }}">{{ $comment->author }}</a></strong>: {{ $comment->comment }}<br />
                                            <small>
                                                 @if($user->role == 'moderator' OR $user->role == 'administrator')
                                                    <a data-comment_delete="{{ $comment->id }}" title="Delete Comment" class="confirm-admin-comment-delete" href="#"><i class="fa fa-remove"></i></a>
                                                 @endif

                                                [{{ $comment->created_at }}]
                                            </small>
                                        </p>
                                    @endforeach
                                @else
                                    <p>No comments to show.</p>
                                @endif

                                <br />

                                <div class="row">
                                    <div class="col-md-8 col-md-offset-2">
                                        <div class="returnedMessage"></div>
                                    </div>
                                </div>

                                <form class="form-horizontal" role="form" method="POST" action="{{ url('/lesson/comment') }}" name="create-comment-form" id="create-comment-form">

                                    {{ csrf_field() }}

                                    <input type="hidden" name="lessonID" value="{{ $lesson->id }}">

                                    <div class="form-group">
                                        <div class="col-md-2">
                                            <strong>Leave a comment: </strong>
                                        </div>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" maxlength="300" name="comment" id="comment" placeholder="Comment">
                                        </div>

                                        <div class="col-md-2">
                                            <button type="submit" name="create-comment-button" id="create-comment-button" class="btn btn-primary"><i class="fa fa-btn fa-sign-in"></i> Comment</button>
                                        </div>
                                    </div>
                                </form>

                                <hr />

                                <div class="align_center">
                                    {{ $comments->links() }}
                                </div>
                        @endif
                    @else
                        <p>{{ $lesson->excerpt }}</p>

                        <hr />

                        @if($user)
                            <h1 class="align_center" style="color: red;">Premium content!</h1>
                            <h3 class="align_center">You need access to this lesson to view its content!</h3>
                            <h4 class="align_center">
                                {{ csrf_field() }}

                                <a title="Buy access to the lesson" data-lesson_access="{{ $lesson->id }}" href="#" class="button button-default button-tiny button-rounded confirm-buy-access-lesson">Buy Access ({{ $lesson->price }}) ezys</a>
                            </h4>
                        @else
                            <h1 class="align_center" style="color: red;">Premium content!</h1>
                            <h3 class="align_center">You need to be signed in and have access to this lesson to view it!</h3>
                            <h4 class="align_center"><a href="{{ url('/login') }}"><i class="fa fa-sign-in"></i> Sign in</a></h4>
                        @endif
                    @endif
                    @if (!empty($lesson->courseID))
                        <hr />
                        <div class="align_center">
                            <a href="{{ url('/course', $lesson->courseID) }}" class="button button-default button-small button-rounded">Back to course</a>
                        </div>
                    @endif
                @else
                    <h1 class="align_center">This lesson does not exists!</h1>
               @endif
			</div>
		</div>
	</div>
@stop