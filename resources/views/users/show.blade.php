@extends('...layouts.default')
@section('content')

	<div id="content_main">
		<div class="section section-white">
			<div class="container">
                @if($userProfile)
                    <div class="row">
                          <div class="col-xs-6">
                                <p><strong><i class="fa fa-user"></i> {{ $userProfile->name }}'s profile</strong>
                                    @if($userProfile->id != $user->id)
                                        @if($user->role == 'moderator' OR $user->role == 'administrator')
                                            <a href="{{ url('/admin/user/manage', $userProfile->id) }}" class="button button-default button-tiny button-rounded">Moderate User</a>
                                        @endif
                                    @endif
                                </p>
                          </div>

                          <div class="col-xs-6">
                            <p class="align_right"><a href="{{ url('/user/ezys/donate', $userProfile->id) }}" class="button button-default button-tiny button-rounded">Donate Ezys</a></p>
                          </div>
                    </div>

                    <hr />

                    <div class="row">
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <p class="align_center">
                                <a href="{{ url('/user/notes', $userProfile->id) }}"><i class="fa fa-sticky-note"></i> {{ $userProfile->name }}'s Notes</a>
                            </p>
                        </div>

                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <p class="align_center">
                                <a href="{{ url('/user/lessons', $userProfile->id) }}"><i class="fa fa-book"></i> {{ $userProfile->name }}'s Lessons</a>
                            </p>
                        </div>

                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <p class="align_center">
                                <a href="{{ url('/user/courses', $userProfile->id) }}"><i class="fa fa-folder-open"></i> {{ $userProfile->name }}'s Courses</a>
                            </p>
                        </div>
                    </div>
                @else
                    <h1 class="align_center">This user does not exists!</h1>
               @endif
			</div>
		</div>
	</div>

@stop