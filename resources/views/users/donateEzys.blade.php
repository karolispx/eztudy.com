@extends('...layouts.default')
@section('content')

	<div id="content_main">
		<div class="section section-white">
			<div class="container">
                @if($userProfile)
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        <div class="returnedMessage4"></div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">Donate Ezys to <a href="{{ url('/user', $userProfile->id) }}">{{ $userProfile->name }}</a></div>
                            <div class="panel-body">
                                <form class="form-horizontal" role="form" method="POST" action="{{ url('/user/ezys/donate') }}" id="donate-ezys-form" name="donate-ezys-form">

                                    {{ csrf_field() }}

                                    <input type="hidden" class="form-control required" maxlength="60" id="userID" name="userID" value="{{ $userProfile->id }}">

                                    <div class="form-group">
                                        <label class="col-md-3 control-label" for="ezysamount">Your Current ezys amount</label>

                                        <div class="col-md-8">
                                            {{ $user->ezys }}
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-3 control-label" for="ezys">Donate ezys amount</label>

                                        <div class="col-md-8">
                                            <input type="number" class="form-control required" maxlength="60" id="ezys" name="ezys">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-md-8 col-md-offset-3">
                                            <button id="donate-ezys-button" type="submit" class="btn btn-primary">
                                                <i class="fa fa-btn fa-money"></i> Donate Ezys
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                @else
                    <h1 class="align_center">This user  does not exists!</h1>
               @endif
			</div>
		</div>
	</div>

@stop