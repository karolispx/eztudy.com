jQuery(document).ready(function($){
    // Lesson course checked
    if(jQuery('input[name="lessonpartofcourse"]:checked').val() == '1') {
        jQuery('#lesson_course_section').show();
    } else {
        jQuery('#lesson_course_section').hide();
    }

    jQuery('input[name="lessonpartofcourse"]').change(function() {
        if(jQuery('input[name="lessonpartofcourse"]:checked').val() == '1') {
            jQuery('#lesson_course_section').show();
        } else {
            jQuery('#lesson_course_section').hide();
        }
    });




    // Give confirm when buying access to paid lesson
    jQuery(document).on('click', '.confirm-buy-access-lesson', function() {
        // Give confirm dialog
        var confirmSubmit = confirm('Are you sure you would like to buy access to this lesson?');

        var _token = jQuery('input[name="_token"]');

        var lessonID = jQuery(this).attr("data-lesson_access");

        // If confirmed
        if(confirmSubmit) {
            // Data to ve set
            var dataSend = {
                '_token': _token.val(),
                'lessonID': lessonID
            };

            // Ajax request
            jQuery.post('/lesson/access', dataSend, function (response) {
                if (response == "success") {
                    // Success
                    alert('You have bought access to this lesson successfully!');
                    location.reload();
                } else {
                    alert(response);
                }
            });
        }
    });




    // Give confirm when deleting lesson
    jQuery(document).on('click', '.confirm-delete-lesson', function() {
        // Give confirm dialog
        var confirmSubmit = confirm('Are you sure you would like to delete this lesson and all of its content permanently?');

        var _token = jQuery('input[name="_token"]');

        var lessonID = jQuery(this).attr("data-lesson_delete");

        // If confirmed
        if(confirmSubmit) {
            // Data to ve set
            var dataSend = {
                '_token': _token.val(),
                'lessonID': lessonID
            };

            // Ajax request
            jQuery.post('/profile/lesson/delete', dataSend, function (response) {
                if (response == "success") {
                    // Success
                    alert('Lesson and all of its content has been deleted successfully!');
                    location.reload();
                } else {
                    alert(response);
                }
            });
        }
    });

    var returnedMessage = jQuery('.returnedMessage');

    var create_comment_form = jQuery('#create-comment-form');
    var create_lesson_form = jQuery('#create-lesson-form');
    var update_lesson_form = jQuery('#update-lesson-form');

    //Update lesson
    // Return false when submitting form, process with ajax
    update_lesson_form.submit(function() {
        return false;
    });

    // If inputs entered, hide error message
    update_lesson_form.keyup(function() {
        returnedMessage.slideUp();
    });

    // Process form when submit is clicked
    jQuery('#update-lesson-button').click(function(e){
        var _token = jQuery('input[name="_token"]');

        var lessonID = jQuery('#lessonID');
        var title = jQuery('#title');
        var excerpt = jQuery('#excerpt');
        var thumbnail = jQuery('#thumbnail');
        var lesson = jQuery('#lesson');
        var tag = jQuery('#tag');

        var inputError = true;

        // Go through inputs if they're empty, highlight them
        jQuery('input[type="text"].required, textarea.required').each(function() {
            if(jQuery.trim(jQuery(this).val()) == '') {
                inputError = false;

                jQuery(this).addClass('error-input-red');
            } else {
                jQuery(this).removeClass('error-input-red');
            }
        });

        // If there are input errors, show error message, otherwise make ajax request
        if(!inputError) {
            e.preventDefault();

            returnedMessage.fadeIn(500).html('<div class="alert alert-danger">Please fill in all information!</div>');
        } else {
            // Data to ve set
            var dataSend = {
                '_token': _token.val(),
                'lessonID': lessonID.val(),
                'title': title.val(),
                'excerpt': excerpt.val(),
                'thumbnail': thumbnail.val(),
                'lesson': lesson.val(),
                'tag': tag.val()
            };

            // Ajax request
            jQuery.post('/profile/lesson/update', dataSend, function(response) {
                if(response == "success") {
                    // Success
                    alert('Lesson has been updated successfully!');
                    window.location.replace('/profile/lessons');
                } else {
                    var result =  jQuery.parseJSON(response);

                    for(var i=0; i < result["errorLocation"].length; ++i){
                        jQuery('#' + result["errorLocation"][i]).addClass('error-input-red');
                    }

                    returnedMessage.fadeIn(500).html('<div class="alert alert-danger">'+result["message"]+'</div>');
                }
            });
        }
    });








    //Create lesson
    // Return false when submitting form, process with ajax
    create_lesson_form.submit(function() {
        return false;
    });

    // If inputs entered, hide error message
    create_lesson_form.keyup(function() {
        returnedMessage.slideUp();
    });

    // Process form when submit is clicked
    jQuery('#create-lesson-button').click(function(e){
        var _token = jQuery('input[name="_token"]');

        var title = jQuery('#title');
        var excerpt = jQuery('#excerpt');
        var thumbnail = jQuery('#thumbnail');
        var lesson = jQuery('#lesson');
        var type = jQuery('input[name="type"]:checked');
        var price = jQuery('#price');
        var lessonpartofcourse = jQuery('input[name="lessonpartofcourse"]:checked');
        var course = jQuery('#course :selected');
        var tag = jQuery('#tag');

        var inputError = true;

        // Go through inputs if they're empty, highlight them
        jQuery('input[type="text"].required, textarea.required').each(function() {
            if(jQuery.trim(jQuery(this).val()) == '') {
                inputError = false;

                jQuery(this).addClass('error-input-red');
            } else {
                jQuery(this).removeClass('error-input-red');
            }
        });

        // If type is paid, ensure price is entered
        if(type.val() == '1' && jQuery.trim(jQuery(price).val()) == '') {
            inputError = false;

            jQuery(price).addClass('error-input-red');
        } else {
            jQuery(price).removeClass('error-input-red');
        }

        // If part of course, ensure course selected
        if(lessonpartofcourse.val() == '1' && jQuery.trim(jQuery(course).val()) == '0') {
            inputError = false;

            jQuery('#course').addClass('error-input-red');
        } else {
            jQuery('#course').removeClass('error-input-red');
        }

        // If there are input errors, show error message, otherwise make ajax request
        if(!inputError) {
            e.preventDefault();

            returnedMessage.fadeIn(500).html('<div class="alert alert-danger">Please fill in all information!</div>');
        } else {
            // Data to ve set
            var dataSend = {
                '_token': _token.val(),
                'title': title.val(),
                'excerpt': excerpt.val(),
                'thumbnail': thumbnail.val(),
                'lesson': lesson.val(),
                'type': type.val(),
                'price': price.val(),
                'lessonpartofcourse': lessonpartofcourse.val(),
                'course': course.val(),
                'tag': tag.val()
            };

            // Ajax request
            jQuery.post('/profile/lesson/create', dataSend, function(response) {
                if(response == "success") {
                    // Success
                    alert('Lesson has been created successfully!');
                    window.location.replace('/profile/lessons');
                } else {
                    var result =  jQuery.parseJSON(response);

                    for(var i=0; i < result["errorLocation"].length; ++i){
                        jQuery('#' + result["errorLocation"][i]).addClass('error-input-red');
                    }

                    returnedMessage.fadeIn(500).html('<div class="alert alert-danger">'+result["message"]+'</div>');
                }
            });
        }
    });



    //Create comment
    // Return false when submitting form, process with ajax
    create_comment_form.submit(function() {
        return false;
    });

    // If inputs entered, hide error message
    create_comment_form.keyup(function() {
        returnedMessage.slideUp();
    });

    // Process form when submit is clicked
    jQuery('#create-comment-button').click(function(e){
        var _token = jQuery('input[name="_token"]');

        var comment = jQuery('input[name="comment"]');
        var lessonID = jQuery('input[name="lessonID"]');

        var inputError = true;

        // Go through inputs if they're empty, highlight them
        jQuery(comment).each(function() {
            if(jQuery.trim(jQuery(this).val()) == '') {
                inputError = false;

                jQuery(this).addClass('error-input-red');
            } else {
                jQuery(this).removeClass('error-input-red');
            }
        });

        // If there are input errors, show error message, otherwise make ajax request
        if(!inputError) {
            e.preventDefault();

            returnedMessage.fadeIn(500).html('<div class="alert alert-danger">Please fill in all information!</div>');
        } else {
            // Data to ve set
            var dataSend = {
                '_token': _token.val(),
                'comment': comment.val(),
                'lessonID': lessonID.val()
            };

            // Ajax request
            jQuery.post('/lesson/comment', dataSend, function(response) {
                if(response == "success") {
                    // Success
                    alert('Comment has been created successfully!');
                    window.location.replace('/lesson/'+lessonID.val());
                } else {
                    var result =  jQuery.parseJSON(response);

                    for(var i=0; i < result["errorLocation"].length; ++i){
                        jQuery('#' + result["errorLocation"][i]).addClass('error-input-red');
                    }

                    returnedMessage.fadeIn(500).html('<div class="alert alert-danger">'+result["message"]+'</div>');
                }
            });
        }
    });
});