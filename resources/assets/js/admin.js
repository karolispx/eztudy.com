jQuery(document).ready(function($){
    // Manage course
    // Give confirm when deleting course
    jQuery(document).on('click', '.confirm-admin-course-delete', function() {
        // Give confirm dialog
        var confirmSubmit = confirm('Are you sure you would like to delete this course and all of its content?');

        var _token = jQuery('input[name="_token"]');

        var courseID = jQuery(this).attr("data-course_delete");

        // If confirmed
        if(confirmSubmit) {
            // Data to ve set
            var dataSend = {
                '_token': _token.val(),
                'courseID': courseID
            };

            // Ajax request
            jQuery.post('/admin/course/delete', dataSend, function (response) {
                if (response == "success") {
                    // Success
                    alert('You have deleted this course and all of its content successfully!');
                    window.location.replace('/');
                } else {
                    alert(response);
                }
            });}
    });

    var returnedMessageManageCourse = jQuery('.returnedMessage5');
    var admin_manage_course_form = jQuery('#admin-manage-course-form');

    // Return false when submitting form, process with ajax
    admin_manage_course_form.submit(function() {
        return false;
    });

    // If inputs entered, hide error message
    admin_manage_course_form.keyup(function() {
        returnedMessageManageCourse.slideUp();
    });

    // Process form when submit is clicked
    jQuery('#admin-manage-course-button').click(function(e){
        var _token = jQuery('input[name="_token"]');

        var courseID = jQuery('#courseID');
        var title = jQuery('#title');
        var excerpt = jQuery('#excerpt');
        var thumbnail = jQuery('#thumbnail');
        var privacy = jQuery('#privacy :selected');
        var tag = jQuery('#tag');

        var inputError = true;

        // Go through inputs if they're empty, highlight them
        jQuery('input[type="text"].required, textarea.required').each(function() {
            if(jQuery.trim(jQuery(this).val()) == '') {
                inputError = false;

                jQuery(this).addClass('error-input-red');
            } else {
                jQuery(this).removeClass('error-input-red');
            }
        });

        // If there are input errors, show error message, otherwise make ajax request
        if(!inputError) {
            e.preventDefault();

            returnedMessageManageCourse.fadeIn(500).html('<div class="alert alert-danger">Please fill in all information!</div>');
        } else {
            // Data to ve set
            var dataSend = {
                '_token': _token.val(),
                'courseID': courseID.val(),
                'title': title.val(),
                'excerpt': excerpt.val(),
                'thumbnail': thumbnail.val(),
                'privacy': privacy.val(),
                'tag': tag.val()
            };

            // Ajax request
            jQuery.post('/admin/course/manage', dataSend, function(response) {
                if(response == "success") {
                    // Success
                    alert('Course has been updated successfully!');
                    location.reload();
                } else {
                    var result =  jQuery.parseJSON(response);

                    for(var i=0; i < result["errorLocation"].length; ++i){
                        jQuery('#' + result["errorLocation"][i]).addClass('error-input-red');
                    }

                    returnedMessageManageCourse.fadeIn(500).html('<div class="alert alert-danger">'+result["message"]+'</div>');
                }
            });
        }
    });



    // Manage lesson
    // Give confirm when deleting comment
    jQuery(document).on('click', '.confirm-admin-comment-delete', function() {
        // Give confirm dialog
        var confirmSubmit = confirm('Are you sure you would like to delete this comment?');

        var _token = jQuery('input[name="_token"]');

        var commentID = jQuery(this).attr("data-comment_delete");

        // If confirmed
        if(confirmSubmit) {
            // Data to ve set
            var dataSend = {
                '_token': _token.val(),
                'commentID': commentID
            };

            // Ajax request
            jQuery.post('/admin/comment/delete', dataSend, function (response) {
                if (response == "success") {
                    // Success
                    alert('You have deleted this comment successfully!');
                    location.reload();
                } else {
                    alert(response);
                }
            });}
    });

    // Give confirm when deleting lesson
    jQuery(document).on('click', '.confirm-admin-lesson-delete', function() {
        // Give confirm dialog
        var confirmSubmit = confirm('Are you sure you would like to delete this lesson and all of its content?');

        var _token = jQuery('input[name="_token"]');

        var lessonID = jQuery(this).attr("data-lesson_delete");

        // If confirmed
        if(confirmSubmit) {
            // Data to ve set
            var dataSend = {
                '_token': _token.val(),
                'lessonID': lessonID
            };

            // Ajax request
            jQuery.post('/admin/lesson/delete', dataSend, function (response) {
                if (response == "success") {
                    // Success
                    alert('You have deleted this lesson and all of its content successfully!');
                    window.location.replace('/');
                } else {
                    alert(response);
                }
            });}
    });

    var returnedMessageManageLesson = jQuery('.returnedMessage5');
    var admin_manage_lesson_form = jQuery('#admin-manage-lesson-form');

    // Return false when submitting form, process with ajax
    admin_manage_lesson_form.submit(function() {
        return false;
    });

    // If inputs entered, hide error message
    admin_manage_lesson_form.keyup(function() {
        returnedMessageManageLesson.slideUp();
    });

    // Process form when submit is clicked
    jQuery('#admin-manage-lesson-button').click(function(e){
        var _token = jQuery('input[name="_token"]');

        var lessonID = jQuery('#lessonID');
        var title = jQuery('#title');
        var excerpt = jQuery('#excerpt');
        var thumbnail = jQuery('#thumbnail');
        var lesson = jQuery('#lesson');
        var privacy = jQuery('#privacy :selected');
        var tag = jQuery('#tag');

        var inputError = true;

        // Go through inputs if they're empty, highlight them
        jQuery('input[type="text"].required, textarea.required').each(function() {
            if(jQuery.trim(jQuery(this).val()) == '') {
                inputError = false;

                jQuery(this).addClass('error-input-red');
            } else {
                jQuery(this).removeClass('error-input-red');
            }
        });

        // If there are input errors, show error message, otherwise make ajax request
        if(!inputError) {
            e.preventDefault();

            returnedMessageManageLesson.fadeIn(500).html('<div class="alert alert-danger">Please fill in all information!</div>');
        } else {
            // Data to ve set
            var dataSend = {
                '_token': _token.val(),
                'lessonID': lessonID.val(),
                'title': title.val(),
                'excerpt': excerpt.val(),
                'thumbnail': thumbnail.val(),
                'lesson': lesson.val(),
                'privacy': privacy.val(),
                'tag': tag.val()
            };

            // Ajax request
            jQuery.post('/admin/lesson/manage', dataSend, function(response) {
                if(response == "success") {
                    // Success
                    alert('Lesson has been updated successfully!');
                    location.reload();
                } else {
                    var result =  jQuery.parseJSON(response);

                    for(var i=0; i < result["errorLocation"].length; ++i){
                        jQuery('#' + result["errorLocation"][i]).addClass('error-input-red');
                    }

                    returnedMessageManageLesson.fadeIn(500).html('<div class="alert alert-danger">'+result["message"]+'</div>');
                }
            });
        }
    });



    // Manage note
    // Give confirm when deleting note
    jQuery(document).on('click', '.confirm-admin-note-delete', function() {
        // Give confirm dialog
        var confirmSubmit = confirm('Are you sure you would like to delete this note?');

        var _token = jQuery('input[name="_token"]');

        var noteID = jQuery(this).attr("data-note_delete");

        // If confirmed
        if(confirmSubmit) {
            // Data to ve set
            var dataSend = {
                '_token': _token.val(),
                'noteID': noteID
            };

            // Ajax request
            jQuery.post('/admin/note/delete', dataSend, function (response) {
                if (response == "success") {
                    // Success
                    alert('You have deleted this note successfully!');
                    window.location.replace('/');
                } else {
                    alert(response);
                }
            });}
    });

    var returnedMessageManageNote = jQuery('.returnedMessage5');
    var admin_manage_note_form = jQuery('#admin-manage-note-form');

    // Return false when submitting form, process with ajax
    admin_manage_note_form.submit(function() {
        return false;
    });

    // If inputs entered, hide error message
    admin_manage_note_form.keyup(function() {
        returnedMessageManageNote.slideUp();
    });

    // Process form when submit is clicked
    jQuery('#admin-manage-note-button').click(function(e){
        var _token = jQuery('input[name="_token"]');

        var noteID = jQuery('#noteID');
        var note = jQuery('#note');
        var privacy = jQuery('#privacy :selected');

        var inputError = true;

        // Go through inputs if they're empty, highlight them
        jQuery('input[type="text"].required, textarea.required').each(function() {
            if(jQuery.trim(jQuery(this).val()) == '') {
                inputError = false;

                jQuery(this).addClass('error-input-red');
            } else {
                jQuery(this).removeClass('error-input-red');
            }
        });

        // If there are input errors, show error message, otherwise make ajax request
        if(!inputError) {
            e.preventDefault();

            returnedMessageManageNote.fadeIn(500).html('<div class="alert alert-danger">Please fill in all information!</div>');
        } else {
            // Data to ve set
            var dataSend = {
                '_token': _token.val(),
                'noteID': noteID.val(),
                'note': note.val(),
                'privacy': privacy.val()
            };

            // Ajax request
            jQuery.post('/admin/note/manage', dataSend, function(response) {
                if(response == "success") {
                    // Success
                    alert('Note has been updated successfully!');
                    location.reload();
                } else {
                    var result =  jQuery.parseJSON(response);

                    for(var i=0; i < result["errorLocation"].length; ++i){
                        jQuery('#' + result["errorLocation"][i]).addClass('error-input-red');
                    }

                    returnedMessageManageNote.fadeIn(500).html('<div class="alert alert-danger">'+result["message"]+'</div>');
                }
            });
        }
    });



    // Manage user
    // Give confirm when deleting user
    jQuery(document).on('click', '.confirm-admin-user-delete', function() {
        // Give confirm dialog
        var confirmSubmit = confirm('Are you sure you would like to delete this user and all of his content?');

        var _token = jQuery('input[name="_token"]');

        var userID = jQuery(this).attr("data-user_delete");

        // If confirmed
        if(confirmSubmit) {
            // Data to ve set
            var dataSend = {
                '_token': _token.val(),
                'userID': userID
            };

            // Ajax request
            jQuery.post('/admin/user/delete', dataSend, function (response) {
                if (response == "success") {
                    // Success
                    alert('You have deleted this user and all of his content successfully!');
                    window.location.replace('/');
                } else {
                    alert(response);
                }
            });
        }
    });

    var returnedMessageManageUser = jQuery('.returnedMessage5');
    var admin_manage_user_form = jQuery('#admin-manage-user-form');

    // Return false when submitting form, process with ajax
    admin_manage_user_form.submit(function() {
        return false;
    });

    // If inputs entered, hide error message
    admin_manage_user_form.keyup(function() {
        returnedMessageManageUser.slideUp();
    });

    // Process form when submit is clicked
    jQuery('#admin-manage-user-button').click(function(e){
        var _token = jQuery('input[name="_token"]');

        var userID = jQuery('#userID');
        var ezys = jQuery('#ezys');
        var role = jQuery('#role :selected');

        var inputError = true;

        // Go through inputs if they're empty, highlight them
        jQuery('input[type="text"].required, textarea.required').each(function() {
            if(jQuery.trim(jQuery(this).val()) == '') {
                inputError = false;

                jQuery(this).addClass('error-input-red');
            } else {
                jQuery(this).removeClass('error-input-red');
            }
        });

        // If there are input errors, show error message, otherwise make ajax request
        if(!inputError) {
            e.preventDefault();

            returnedMessageManageUser.fadeIn(500).html('<div class="alert alert-danger">Please fill in all information!</div>');
        } else {
            // Data to ve set
            var dataSend = {
                '_token': _token.val(),
                'userID': userID.val(),
                'ezys': ezys.val(),
                'role': role.val()
            };

            // Ajax request
            jQuery.post('/admin/user/manage', dataSend, function(response) {
                if(response == "success") {
                    // Success
                    alert('User has been updated successfully!');
                    location.reload();
                } else {
                    var result =  jQuery.parseJSON(response);

                    for(var i=0; i < result["errorLocation"].length; ++i){
                        jQuery('#' + result["errorLocation"][i]).addClass('error-input-red');
                    }

                    returnedMessageManageUser.fadeIn(500).html('<div class="alert alert-danger">'+result["message"]+'</div>');
                }
            });
        }
    });
});