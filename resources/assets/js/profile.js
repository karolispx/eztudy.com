jQuery(document).ready(function($){
    var returnedMessage4 = jQuery('.returnedMessage4');
    var returnedMessage1 = jQuery('.returnedMessage1');
    var returnedMessage3 = jQuery('.returnedMessage3');

    var donate_ezys_form = jQuery('#donate-ezys-form');
    var buy_ezys_form = jQuery('#buy-ezys-form');
    var update_password_form = jQuery('#update-password-form');

    // Donate ezys
    // Return false when submitting form, process with ajax
    donate_ezys_form.submit(function() {
        return false;
    });

    // If inputs entered, hide error message
    donate_ezys_form.keyup(function() {
        returnedMessage4.slideUp();
    });

    // Process form when submit is clicked
    jQuery('#donate-ezys-button').click(function(e){
        var _token = jQuery('input[name="_token"]');
        var ezys = jQuery('#ezys');
        var userID = jQuery('#userID');

        var inputError = true;

        // Go through inputs if they're empty, highlight them
        jQuery(ezys).each(function() {
            if(jQuery.trim(jQuery(this).val()) == '') {
                inputError = false;

                jQuery(this).addClass('error-input-red');
            } else {
                jQuery(this).removeClass('error-input-red');
            }
        });

        // If there are input errors, show error message, otherwise make ajax request
        if(!inputError) {
            e.preventDefault();

            returnedMessage4.fadeIn(500).html('<div class="alert alert-danger">Please fill in all information!</div>');
        } else {
            // Data to ve set
            var dataSend = {
                '_token': _token.val(),
                'ezys': ezys.val(),
                'userID': userID.val()
            };

            // Ajax request
            jQuery.post('/user/ezys/donate', dataSend, function(response) {
                if(response == "success") {
                    // Success
                    alert('You have successfully donated '+ezys.val()+' ezys to the user!');
                    location.reload();
                } else {
                    var result =  jQuery.parseJSON(response);

                    for(var i=0; i < result["errorLocation"].length; ++i){
                        jQuery('#' + result["errorLocation"][i]).addClass('error-input-red');
                    }

                    returnedMessage4.fadeIn(500).html('<div class="alert alert-danger">'+result["message"]+'</div>');
                }
            });
        }
    });




    // Buy ezys
    // Return false when submitting form, process with ajax
    buy_ezys_form.submit(function() {
        return false;
    });

    // If inputs entered, hide error message
    buy_ezys_form.keyup(function() {
        returnedMessage1.slideUp();
    });

    // Process form when submit is clicked
    jQuery('#buy-ezys-button').click(function(e){
        var _token = jQuery('input[name="_token"]');

        var ezys = jQuery('#ezys');

        var inputError = true;

        // Go through inputs if they're empty, highlight them
        jQuery(ezys).each(function() {
            if(jQuery.trim(jQuery(this).val()) == '') {
                inputError = false;

                jQuery(this).addClass('error-input-red');
            } else {
                jQuery(this).removeClass('error-input-red');
            }
        });

        // If there are input errors, show error message, otherwise make ajax request
        if(!inputError) {
            e.preventDefault();

            returnedMessage1.fadeIn(500).html('<div class="alert alert-danger">Please fill in all information!</div>');
        } else {
            // Data to ve set
            var dataSend = {
                '_token': _token.val(),
                'ezys': ezys.val()
            };

            // Ajax request
            jQuery.post('/profile/ezys', dataSend, function(response) {
                if(response == "success") {
                    // Success
                    alert('You have successfully bought '+ezys.val()+' ezys!');
                    location.reload();
                } else {
                    var result =  jQuery.parseJSON(response);

                    for(var i=0; i < result["errorLocation"].length; ++i){
                        jQuery('#' + result["errorLocation"][i]).addClass('error-input-red');
                    }

                    returnedMessage1.fadeIn(500).html('<div class="alert alert-danger">'+result["message"]+'</div>');
                }
            });
        }
    });



    // Update password
    // Return false when submitting form, process with ajax
    update_password_form.submit(function() {
        return false;
    });

    // If inputs entered, hide error message
    update_password_form.keyup(function() {
        returnedMessage3.slideUp();
    });

    // Process form when submit is clicked
    jQuery('#update-password-button').click(function(e){
        var _token = jQuery('input[name="_token"]');

        var password = jQuery('#password');
        var password1 = jQuery('#password1');
        var password2 = jQuery('#password2');

        var inputError = true;

        // Go through inputs if they're empty, highlight them
        jQuery('#password, #password1, #password2').each(function() {
            if(jQuery.trim(jQuery(this).val()) == '') {
                inputError = false;

                jQuery(this).addClass('error-input-red');
            } else {
                jQuery(this).removeClass('error-input-red');
            }
        });

        // If there are input errors, show error message, otherwise make ajax request
        if(!inputError) {
            e.preventDefault();

            returnedMessage3.fadeIn(500).html('<div class="alert alert-danger">Please fill in all information!</div>');
        } else {
            // Data to ve set
            var dataSend = {
                '_token': _token.val(),
                'password': password.val(),
                'password1': password1.val(),
                'password2': password2.val()
            };

            // Ajax request
            jQuery.post('/profile/password', dataSend, function(response) {
                if(response == "success") {
                    // Success
                    alert('You have updated your password successfully!');
                    location.reload();
                } else {
                    var result =  jQuery.parseJSON(response);

                    for(var i=0; i < result["errorLocation"].length; ++i){
                        jQuery('#' + result["errorLocation"][i]).addClass('error-input-red');
                    }

                    returnedMessage3.fadeIn(500).html('<div class="alert alert-danger">'+result["message"]+'</div>');
                }
            });
        }
    });




});