jQuery(document).ready(function($){


    // Give confirm when deleting lesson
    jQuery(document).on('click', '.confirm-delete-note', function() {
        // Give confirm dialog
        var confirmSubmit = confirm('Are you sure you would like to delete this note permanently?');

        var _token = jQuery('input[name="_token"]');

        var noteID = jQuery(this).attr("data-note_delete");

        // If confirmed
        if(confirmSubmit) {
            // Data to ve set
            var dataSend = {
                '_token': _token.val(),
                'noteID': noteID
            };

            // Ajax request
            jQuery.post('/profile/note/delete', dataSend, function (response) {
                if (response == "success") {
                    // Success
                    alert('Note has been deleted successfully!');
                    location.reload();
                } else {
                    alert(response);
                }
            });
        }
    });



    var returnedMessage = jQuery('.returnedMessage');
    var returnedMessage7 = jQuery('.returnedMessage7');

    var create_note_form = jQuery('#create-note-form');
    var create_note_form2 = jQuery('#create-note-form2');
    var update_note_form = jQuery('#update-note-form');

    //Update note
    // Return false when submitting form, process with ajax
    update_note_form.submit(function() {
        return false;
    });

    // If inputs entered, hide error message
    update_note_form.keyup(function() {
        returnedMessage.slideUp();
    });

    // Process form when submit is clicked
    jQuery('#update-note-button').click(function(e){
        var _token = jQuery('input[name="_token"]');

        var noteID = jQuery('#noteID');
        var note = jQuery('#note');
        var privacy = jQuery('input[name="privacy"]:checked');

        var inputError = true;

        // Go through inputs if they're empty, highlight them
        jQuery('input[type="text"].required, textarea.required').each(function() {
            if(jQuery.trim(jQuery(this).val()) == '') {
                inputError = false;

                jQuery(this).addClass('error-input-red');
            } else {
                jQuery(this).removeClass('error-input-red');
            }
        });

        // If there are input errors, show error message, otherwise make ajax request
        if(!inputError) {
            e.preventDefault();

            returnedMessage.fadeIn(500).html('<div class="alert alert-danger">Please fill in all information!</div>');
        } else {
            // Data to ve set
            var dataSend = {
                '_token': _token.val(),
                'noteID': noteID.val(),
                'note': note.val(),
                'privacy': privacy.val()
            };

            // Ajax request
            jQuery.post('/profile/note/update', dataSend, function(response) {
                if(response == "success") {
                    // Success
                    alert('Note has been updated successfully!');
                    window.location.replace('/profile/notes');
                } else {
                    var result =  jQuery.parseJSON(response);

                    for(var i=0; i < result["errorLocation"].length; ++i){
                        jQuery('#' + result["errorLocation"][i]).addClass('error-input-red');
                    }

                    returnedMessage.fadeIn(500).html('<div class="alert alert-danger">'+result["message"]+'</div>');
                }
            });
        }
    });








    //Create note 2
    // Return false when submitting form, process with ajax
    create_note_form2.submit(function() {
        return false;
    });

    // If inputs entered, hide error message
    create_note_form2.keyup(function() {
        returnedMessage7.slideUp();
    });

    // Process form when submit is clicked
    jQuery('#create-note-button2').click(function(e){
        var _token = jQuery('input[name="_token"]');

        var note2 = jQuery('#note2');
        var privacy2 = jQuery('input[name="privacy2"]:checked');
        var lessonID = jQuery('input[name="lessonID"]');

        var inputError = true;

        // Go through inputs if they're empty, highlight them
        jQuery(note2).each(function() {
            if(jQuery.trim(jQuery(this).val()) == '') {
                inputError = false;

                jQuery(this).addClass('error-input-red');
            } else {
                jQuery(this).removeClass('error-input-red');
            }
        });

        // If there are input errors, show error message, otherwise make ajax request
        if(!inputError) {
            e.preventDefault();

            returnedMessage7.fadeIn(500).html('<div class="alert alert-danger">Please fill in all information!</div>');
        } else {
            // Data to ve set
            var dataSend = {
                '_token': _token.val(),
                'note': note2.val(),
                'privacy': privacy2.val(),
                'lessonID': lessonID.val()
            };

            // Ajax request
            jQuery.post('/profile/note/create', dataSend, function(response) {
                if(response == "success") {
                    // Success
                    returnedMessage7.fadeIn(500).html('<div class="alert alert-success">Note has been created successfully!</div>');

                    jQuery('#note2').val('');

                } else {
                    var result =  jQuery.parseJSON(response);

                    for(var i=0; i < result["errorLocation"].length; ++i){
                        jQuery('#' + result["errorLocation"][i]).addClass('error-input-red');
                    }

                    returnedMessage7.fadeIn(500).html('<div class="alert alert-danger">'+result["message"]+'</div>');
                }
            });
        }
    });






    //Create note
    // Return false when submitting form, process with ajax
    create_note_form.submit(function() {
        return false;
    });

    // If inputs entered, hide error message
    create_note_form.keyup(function() {
        returnedMessage.slideUp();
    });

    // Process form when submit is clicked
    jQuery('#create-note-button').click(function(e){
        var _token = jQuery('input[name="_token"]');

        var note = jQuery('#note');
        var privacy = jQuery('input[name="privacy"]:checked');

        var inputError = true;

        // Go through inputs if they're empty, highlight them
        jQuery('input[type="text"].required, textarea.required').each(function() {
            if(jQuery.trim(jQuery(this).val()) == '') {
                inputError = false;

                jQuery(this).addClass('error-input-red');
            } else {
                jQuery(this).removeClass('error-input-red');
            }
        });

        // If there are input errors, show error message, otherwise make ajax request
        if(!inputError) {
            e.preventDefault();

            returnedMessage.fadeIn(500).html('<div class="alert alert-danger">Please fill in all information!</div>');
        } else {
            // Data to ve set
            var dataSend = {
                '_token': _token.val(),
                'note': note.val(),
                'privacy': privacy.val()
            };

            // Ajax request
            jQuery.post('/profile/note/create', dataSend, function(response) {
                if(response == "success") {
                    // Success
                    alert('Note has been created successfully!');
                    window.location.replace('/profile/notes');
                } else {
                    var result =  jQuery.parseJSON(response);

                    for(var i=0; i < result["errorLocation"].length; ++i){
                        jQuery('#' + result["errorLocation"][i]).addClass('error-input-red');
                    }

                    returnedMessage.fadeIn(500).html('<div class="alert alert-danger">'+result["message"]+'</div>');
                }
            });
        }
    });
});