jQuery(document).ready(function($){

    // Give confirm when removing lesson from course
    jQuery(document).on('click', '.confirm-remove-lesson', function() {
        // Give confirm dialog
        var confirmSubmit = confirm('Are you sure you would like to remove this lesson from this course?');

        var _token = jQuery('input[name="_token"]');

        var lessonID = jQuery(this).attr("data-lesson_remove");
        var courseID = jQuery(this).attr("data-course_remove");

        // If confirmed
        if(confirmSubmit) {
            // Data to ve set
            var dataSend = {
                '_token': _token.val(),
                'lessonID': lessonID,
                'courseID': courseID
            };

            // Ajax request
            jQuery.post('/profile/course/lesson/delete', dataSend, function (response) {
                if (response == "success") {
                    // Success
                    alert('Lesson has been removed from the course successfully!');
                    location.reload();
                } else {
                    alert(response);
                }
            });
        }
    });



    // Give confirm when buying access to paid course
    jQuery(document).on('click', '.confirm-buy-access-course', function() {
        // Give confirm dialog
        var confirmSubmit = confirm('Are you sure you would like to buy access to this course?');

        var _token = jQuery('input[name="_token"]');

        var courseID = jQuery(this).attr("data-course_access");

        // If confirmed
        if(confirmSubmit) {
            // Data to ve set
            var dataSend = {
                '_token': _token.val(),
                'courseID': courseID
            };

            // Ajax request
            jQuery.post('/course/access', dataSend, function (response) {
                if (response == "success") {
                    // Success
                    alert('You have bought access to this course successfully!');
                    location.reload();
                } else {
                    alert(response);
                }
            });
        }
    });



    // Give confirm when deleting course
    jQuery(document).on('click', '.confirm-delete-course', function() {
        // Give confirm dialog
        var confirmSubmit = confirm('Are you sure you would like to delete this course and all of its content permanently?');

        var _token = jQuery('input[name="_token"]');

        var courseID = jQuery(this).attr("data-course_delete");

        // If confirmed
        if(confirmSubmit) {
            // Data to ve set
            var dataSend = {
                '_token': _token.val(),
                'courseID': courseID
            };

            // Ajax request
            jQuery.post('/profile/course/delete', dataSend, function (response) {
                if (response == "success") {
                    // Success
                    alert('Course and all of its content has been deleted successfully!');
                    location.reload();
                } else {
                    alert(response);
                }
            });
        }
    });


    var returnedMessage = jQuery('.returnedMessage');

    var create_course_form = jQuery('#create-course-form');
    var update_course_form = jQuery('#update-course-form');

    //Update course
    // Return false when submitting form, process with ajax
    update_course_form.submit(function() {
        return false;
    });

    // If inputs entered, hide error message
    update_course_form.keyup(function() {
        returnedMessage.slideUp();
    });

    // Process form when submit is clicked
    jQuery('#update-course-button').click(function(e){
        var _token = jQuery('input[name="_token"]');

        var courseID = jQuery('#courseID');
        var title = jQuery('#title');
        var excerpt = jQuery('#excerpt');
        var thumbnail = jQuery('#thumbnail');
        var tag = jQuery('#tag');

        var inputError = true;

        // Go through inputs if they're empty, highlight them
        jQuery('input[type="text"].required, textarea.required').each(function() {
            if(jQuery.trim(jQuery(this).val()) == '') {
                inputError = false;

                jQuery(this).addClass('error-input-red');
            } else {
                jQuery(this).removeClass('error-input-red');
            }
        });

        // If there are input errors, show error message, otherwise make ajax request
        if(!inputError) {
            e.preventDefault();

            returnedMessage.fadeIn(500).html('<div class="alert alert-danger">Please fill in all information!</div>');
        } else {
            // Data to ve set
            var dataSend = {
                '_token': _token.val(),
                'courseID': courseID.val(),
                'title': title.val(),
                'excerpt': excerpt.val(),
                'thumbnail': thumbnail.val(),
                'tag': tag.val()
            };

            // Ajax request
            jQuery.post('/profile/course/update', dataSend, function(response) {
                if(response == "success") {
                    // Success
                    alert('Course has been updated successfully!');
                    window.location.replace('/profile/courses');
                } else {
                    var result =  jQuery.parseJSON(response);

                    for(var i=0; i < result["errorLocation"].length; ++i){
                        jQuery('#' + result["errorLocation"][i]).addClass('error-input-red');
                    }

                    returnedMessage.fadeIn(500).html('<div class="alert alert-danger">'+result["message"]+'</div>');
                }
            });
        }
    });








    //Create course
    // Return false when submitting form, process with ajax
    create_course_form.submit(function() {
        return false;
    });

    // If inputs entered, hide error message
    create_course_form.keyup(function() {
        returnedMessage.slideUp();
    });

    // Process form when submit is clicked
    jQuery('#create-course-button').click(function(e){
        var _token = jQuery('input[name="_token"]');

        var title = jQuery('#title');
        var excerpt = jQuery('#excerpt');
        var thumbnail = jQuery('#thumbnail');
        var type = jQuery('input[name="type"]:checked');
        var price = jQuery('#price');
        var tag = jQuery('#tag');

        var inputError = true;

        // Go through inputs if they're empty, highlight them
        jQuery('input[type="text"].required, textarea.required').each(function() {
            if(jQuery.trim(jQuery(this).val()) == '') {
                inputError = false;

                jQuery(this).addClass('error-input-red');
            } else {
                jQuery(this).removeClass('error-input-red');
            }
        });

        // If type is paid, ensure price is entered
        if(type.val() == '1' && jQuery.trim(jQuery(price).val()) == '') {
            inputError = false;

            jQuery(price).addClass('error-input-red');
        } else {
            jQuery(price).removeClass('error-input-red');
        }

        // If there are input errors, show error message, otherwise make ajax request
        if(!inputError) {
            e.preventDefault();

            returnedMessage.fadeIn(500).html('<div class="alert alert-danger">Please fill in all information!</div>');
        } else {
            // Data to ve set
            var dataSend = {
                '_token': _token.val(),
                'title': title.val(),
                'excerpt': excerpt.val(),
                'thumbnail': thumbnail.val(),
                'type': type.val(),
                'price': price.val(),
                'tag': tag.val()
            };

            // Ajax request
            jQuery.post('/profile/course/create', dataSend, function(response) {
                if(response == "success") {
                    // Success
                    alert('Course has been created successfully!');
                    window.location.replace('/profile/courses');
                } else {
                    var result =  jQuery.parseJSON(response);

                    for(var i=0; i < result["errorLocation"].length; ++i){
                        jQuery('#' + result["errorLocation"][i]).addClass('error-input-red');
                    }

                    returnedMessage.fadeIn(500).html('<div class="alert alert-danger">'+result["message"]+'</div>');
                }
            });
        }
    });
});