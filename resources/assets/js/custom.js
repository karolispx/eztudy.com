jQuery(document).ready(function($){
    // Disallow entering special characters into tag input
    jQuery('#tag').keyup(function(){
        var value = tag_strip(jQuery(this).val());
        jQuery(this).val(value);
        return false;
    });

    // Notes slide out
    if(jQuery('#notes-slideout').length) {
        jQuery('#notes-slideout').BootSideMenu({side:"right"});
    }

    // Price radio checked
    if(jQuery('input[name="type"]:checked').val() == '1') {
        jQuery('#price_section').show();
    } else {
        jQuery('#price_section').hide();
    }

    jQuery('input[name="type"]').change(function() {
        if(jQuery('input[name="type"]:checked').val() == '1') {
            jQuery('#price_section').show();
        } else {
            jQuery('#price_section').hide();
        }
    });

    // Give confirm when deleting
    jQuery('.confirm-delete').on('click', function () {
        return confirm('Are you sure you would like to delete this item permanently?');
    });
});

// Converts string to slug
function tag_strip(str) {
    str = str.replace(/^\s+|\s+$/g, ''); // trim
    str = str.toLowerCase();

    // remove accents, swap ñ for n, etc
    var from = "àáäâèéëêìíïîòóöôùúüûñç·/-,:;";
    var to   = "aaaaeeeeiiiioooouuuunc------";

    for (var i=0, l=from.length ; i<l ; i++) {
        str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
    }

    str = str.replace(/[^a-z0-9 _]/g, '').replace(/\s+/g, '_').replace(/_+/g, '_');

    return str;
}